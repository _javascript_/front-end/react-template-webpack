# React-Template-Webpack

Тут будет описание проекта.

# 🛠 Технологии:

- 🔱 **Webpack**
- 🛶 **SASS loader**
- 🌀 **TypeScript**
- ⚛️ **ReactJS 18**
- ⚛️ **React Router v6**
- **Effector**
- **Framer Motion**
- **i18next**
- 🧹 **ESLint**
- **Prettier**
- **Jest**
- **Storybook**
- **Docker**
- **Nodejs**

### Подробнее о проекте

- **[TypeScript](https://www.typescriptlang.org/)** — Типизированный JS.

- **[Effector](https://effector.dev/)** — Легковестный стейтменеджер, с атомарными стейтами. (Хорошая альтернатива Redux)

- **[Framer Motion](https://www.framer.com/developers/)** — это инструмент, созданный для интерактивного дизайна.

- **[React Router v6](https://reactrouter.com/docs/en/v6/getting-started/overview)** — Роутинг в нашем реакт приложении.

- **[BEM](https://ru.bem.info/methodology/quick-start/)** — Сss методология. БЭМ (Блок, Элемент, Модификатор) — компонентный подход к веб-разработке. В его основе лежит принцип разделения интерфейса на независимые блоки. Он позволяет легко и быстро разрабатывать интерфейсы любой сложности и повторно использовать существующий код, избегая «Copy-Paste».

- **[i18next](https://react.i18next.com/)** — Позволяет реализовать многоязычность.

- **[Docker](https://www.docker.com/)** — Программное обеспечение для автоматизации развёртывания.

- **[Vite](https://vitejs.dev/)** — Сборщик проектов, хорошая альтернатива вебпаку.

- **[Nodejs](https://nodejs.org/dist/latest-v16.x/docs/api/)** — Использовал для написания простеньких скриптов.

  1. Альясы для Vite, скрипт при старте читает попку src и на основе содержимого создаётся конфиг.
  2. Написал CLI, при запуске скрипта позволяет выбрать элемент, и в зависимости от варианты создаёт стандартную файловую структору выбранного элемента.

- 📈 Архитектура -> feature-sliced
  [Документация](https://feature-sliced.design/)

# 👨‍👩‍👦‍👦 Зависимости

```json
"devDependencies": {
    "@marshallofsound/webpack-asset-relocator-loader": "^0.5.0",
    "@types/compose-function": "^0.0.30",
    "@types/jest": "^29.0.1",
    "@types/react": "^18.0.18",
    "@types/react-dom": "^18.0.6",
    "@types/webpack-env": "^1.18.0",
    "@typescript-eslint/eslint-plugin": "^5.36.1",
    "@typescript-eslint/parser": "^5.36.1",
    "cross-env": "^7.0.3",
    "css-loader": "^6.7.1",
    "dotenv": "^16.0.2",
    "eslint": "^8.23.0",
    "eslint-config-airbnb": "^19.0.4",
    "eslint-config-airbnb-typescript": "^17.0.0",
    "eslint-config-prettier": "^8.5.0",
    "eslint-plugin-import": "^2.26.0",
    "eslint-plugin-prettier": "^4.2.1",
    "file-loader": "^6.2.0",
    "fork-ts-checker-webpack-plugin": "^7.2.13",
    "html-webpack-plugin": "^5.5.0",
    "jest": "^29.0.3",
    "mini-css-extract-plugin": "^2.6.1",
    "prettier": "^2.7.1",
    "sass": "^1.54.8",
    "sass-loader": "^13.0.2",
    "style-loader": "^3.3.1",
    "ts-jest": "^29.0.0",
    "ts-loader": "^9.3.1",
    "typescript": "^4.8.2",
    "webpack": "^5.74.0",
    "webpack-cli": "^4.10.0",
    "webpack-dev-server": "^4.10.1"
  },
"dependencies": {
    "compose-function": "^3.0.3",
    "effector": "^22.3.0",
    "effector-logger": "^0.13.4",
    "effector-react": "^22.1.6",
    "i18next": "^21.9.1",
    "immer": "^9.0.15",
    "normalizr": "^3.6.2",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-helmet-async": "^1.3.0",
    "react-i18next": "^11.18.5",
    "react-router": "^6.3.0",
    "react-router-dom": "^6.3.0"
}
```
