import { useCallback, useMemo } from "react";

import { PAGES_META_DATA } from "shared/config/pages";
import { appContextModel } from "entities/app-context";
import { viewerModel } from "entities/viewer";

import { Level } from "shared/config/access";
import { useCurrentPage } from "shared/hooks/useCurrentPage";
import { useAppDispatch } from "shared/lib/store";

export function useLayout() {
  const dispatch = useAppDispatch();
  const currentPage = useCurrentPage();

  const access = viewerModel.selectors.useAccess();
  const showLeftbar = appContextModel.selectors.useShowLeftbar();

  const { layout } = PAGES_META_DATA[currentPage] ?? {};

  const layoutString = useMemo(() => {
    if (!layout) return "";
    if (typeof layout === "string") return layout;
    return layout[Math.max(...access) as Level] ?? "only-content";
  }, [layout, access]);

  const { isLeftbar } = useMemo(() => {
    const isHeader = layoutString.includes("header");
    const isLeftbar = layoutString.includes("leftbar");
    const isFooter = layoutString.includes("footer");
    return { isHeader, isLeftbar, isFooter };
  }, [layoutString]);

  const shift = useMemo(() => {
    return isLeftbar && showLeftbar;
  }, [isLeftbar, showLeftbar]);

  const toggleLeftbar = useCallback(() => {
    dispatch(appContextModel.actions.toggleLeftbar());
  }, []);

  return {
    toggleLeftbar,
    shift,
    isLeftbar,
  };
}
