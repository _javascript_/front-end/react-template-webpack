import { Button } from "@mui/material";
import { motion } from "framer-motion";
import React from "react";

import { bem } from "shared/lib/bem";
import { clsx } from "shared/lib/clsx";

import { VARIANTS, variants } from "../../utils";

const leftbarBem = bem("leftbar");

type LeftbarProps = { shift: boolean; toggleLeftbar: () => void };
function LeftbarComponent(props: LeftbarProps) {
  const { shift, toggleLeftbar } = props;
  const classes = clsx(
    leftbarBem(),
    leftbarBem({ open: shift, close: !shift }),
  );

  return (
    <motion.aside
      className={classes}
      variants={variants}
      animate={shift ? VARIANTS.OPEN : VARIANTS.CLOSE}
    >
      Leftbar
      <Button variant="contained" onClick={toggleLeftbar}>
        Toggle Leftbar
      </Button>
    </motion.aside>
  );
}

export const Leftbar = React.memo(LeftbarComponent);
