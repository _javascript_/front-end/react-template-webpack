import { motion } from "framer-motion";
import React from "react";

import { Nav } from "features/navigation";
import { bem } from "shared/lib/bem";
import { clsx } from "shared/lib/clsx";


import { variants, VARIANTS } from "../../utils";

const headerBem = bem("header");

type HeaderProps = {
  shift: boolean;
};
function HeaderComponent(props: HeaderProps) {
  const { shift } = props;

  const classes = clsx(headerBem(), headerBem({ shift }));

  return (
    <motion.header
      variants={variants}
      animate={shift ? VARIANTS.SHIFT : VARIANTS.UN_SHIFT}
      className={classes}
    >
      <Nav />
    </motion.header>
  );
}

export const Header = React.memo(HeaderComponent);
