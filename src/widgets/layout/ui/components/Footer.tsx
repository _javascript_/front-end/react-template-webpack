import { motion } from "framer-motion";
import React from "react";

import { bem } from "shared/lib/bem";
import { clsx } from "shared/lib/clsx";

import { variants, VARIANTS } from "../../utils";

const footerBem = bem("footer");

type FooterProps = { shift: boolean };
function FooterComponent(props: FooterProps) {
  const { shift } = props;
  const classes = clsx(footerBem(), footerBem({ shift }));

  return (
    <motion.footer
      className={classes}
      variants={variants}
      animate={shift ? VARIANTS.SHIFT : VARIANTS.UN_SHIFT}
    >
      Footer
    </motion.footer>
  );
}

export const Footer = React.memo(FooterComponent);
