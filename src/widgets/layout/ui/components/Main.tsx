import { motion } from "framer-motion";
import React from "react";
import { Outlet } from "react-router";

import { PAGES_META_DATA } from "shared/config/pages";
import { useCurrentPage } from "shared/hooks/useCurrentPage";
import { bem } from "shared/lib/bem";
import { clsx } from "shared/lib/clsx";

import { variants, VARIANTS } from "../../utils";

const mainBem = bem("main");

type MainProps = { shift: boolean };
function MainComponent(props: MainProps) {
  const { shift } = props;

  const currentPage = useCurrentPage();
  const { pageName } = PAGES_META_DATA[currentPage] ?? {};

  const classes = clsx(mainBem(), mainBem({ shift }), `page-${pageName}`);

  return (
    <motion.main
      className={classes}
      variants={variants}
      animate={shift ? VARIANTS.SHIFT : VARIANTS.UN_SHIFT}
    >
      <Outlet />
    </motion.main>
  );
}

export const Main = React.memo(MainComponent);
