import React from "react";
import { appContextModel } from "entities/app-context";

import { Main } from "./components";

namespace Layout {
  export type Props = {
    Header?: React.FC;
    Leftbar?: React.FC;
    Footer?: React.FC;
  };
}
function LayoutComponent(props: Layout.Props) {
  const { Footer, Header, Leftbar } = props;
  const { isFooter, isHeader, isLeftbar, shift } = appContextModel.useLayout();

  return (
    <>
      {isHeader && <Header />}
      {isLeftbar && <Leftbar />}
      <Main shift={shift} />
      {isFooter && <Footer />}
    </>
  );
}

export const Layout = React.memo(LayoutComponent);
