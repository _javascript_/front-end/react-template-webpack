const KEY = "show_leftbar";

export function getShowLeftbar() {
  const shiftLeftbar = localStorage.getItem(KEY);
  if (!shiftLeftbar) return false;
  return shiftLeftbar === "true";
}

export function setShowLeftbar(showLeftbar: boolean) {
  localStorage.setItem(KEY, showLeftbar ? "true" : "false");
}
