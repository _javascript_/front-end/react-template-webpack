import { Variants } from "framer-motion";

export const VARIANTS = {
  SHIFT: "shift",
  UN_SHIFT: "unshift",
  OPEN: "open",
  CLOSE: "close",
};

export const variants: Variants = {
  [VARIANTS.SHIFT]: {
    marginLeft: `var(--leftbar-open-width)`,
  },
  [VARIANTS.UN_SHIFT]: {
    marginLeft: `var(--leftbar-close-width)`,
  },
  [VARIANTS.OPEN]: {
    width: `var(--leftbar-open-width)`,
  },
  [VARIANTS.CLOSE]: {
    width: `var(--leftbar-close-width)`,
  },
};
