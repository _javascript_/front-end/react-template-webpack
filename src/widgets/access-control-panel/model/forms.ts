import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";

import * as yup from "yup";

import { viewerModel } from "entities/viewer";
import { ACCESS_LEVELS, Level } from "shared/config/access";


type AddLevelFields = {
  field: Level;
};

const schemaAddLevelForm = yup
  .object({
    field: yup
      .number()
      .positive()
      .integer()
      .max(Math.max(...Object.values(ACCESS_LEVELS)))
      .min(Math.min(...Object.values(ACCESS_LEVELS)))
      .required(),
  })
  .required();

export const useAddLevelForm = (initialValues?: AddLevelFields) => {
  const dispatch = useDispatch();
  const methods = useForm<AddLevelFields>({
    resolver: yupResolver(schemaAddLevelForm),
    defaultValues: initialValues,
  });
  const onSubmit = methods.handleSubmit((data) => {
    dispatch(viewerModel.actions.addLevel(data.field));
  });

  return { onSubmit, methods };
};

type RemoveLevelFields = {
  field: Level;
};

const schemaRemoveLevelForm = yup
  .object({
    field: yup
      .number()
      .positive()
      .integer()
      .max(Math.max(...Object.values(ACCESS_LEVELS)))
      .min(Math.min(...Object.values(ACCESS_LEVELS)))
      .required(),
  })
  .required();

export const useRemoveLevelForm = (initialValues?: RemoveLevelFields) => {
  const dispatch = useDispatch();

  const methods = useForm<RemoveLevelFields>({
    resolver: yupResolver(schemaRemoveLevelForm),
    defaultValues: initialValues,
  });
  const onSubmit = methods.handleSubmit((data) => {
    dispatch(viewerModel.actions.removeLevel(data.field));
  });

  return { onSubmit, methods };
};
