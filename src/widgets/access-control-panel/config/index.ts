export const SLICE_NAME = "access-control-panel";

export const INITIAL_STATE_VEIW_MODEL: {
  addLevelField: number;
  removeLevelField: number;
} = {
  addLevelField: 1,
  removeLevelField: 1,
};
