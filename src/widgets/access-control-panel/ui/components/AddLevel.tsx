import React from "react";
import { Controller } from "react-hook-form";
import { ACCESS_LEVELS } from "shared/config/access";

import { clsx } from "shared/lib";

import { forms } from "../../model";

import { bem } from "../../utils";

export function AddLevelForm() {
  const {
    methods: { control },
    onSubmit,
  } = forms.useAddLevelForm({ field: 1 });
  const classes = clsx(bem("section"), bem("section", { "add-level": true }));
  return (
    <form onSubmit={onSubmit} className={classes}>
      <Controller
        name="field"
        control={control}
        render={({ field, fieldState: { error } }) => {
          return (
            <div>
              <input
                {...field}
                type="number"
                max={Math.max(...Object.values(ACCESS_LEVELS))}
                min={Math.min(...Object.values(ACCESS_LEVELS))}
              />
              {error && <p>{error.message}</p>}
            </div>
          );
        }}
      />
      <button type="submit">add</button>
    </form>
  );
}

// function Input({ control }: any) {
//   const firstName = useController({
//     control,
//     name: "firstName",
//     defaultValue: "default",
//   });
//   return (

//   );
// }
