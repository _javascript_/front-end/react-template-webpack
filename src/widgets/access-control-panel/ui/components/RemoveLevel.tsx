import React from "react";
import { Controller } from "react-hook-form";
import { ACCESS_LEVELS } from "shared/config/access";

import { forms } from "../../model";

export function RemoveLevelForm() {
  const {
    methods: { control },
    onSubmit,
  } = forms.useRemoveLevelForm({ field: 1 });

  return (
    <form onSubmit={onSubmit}>
      <Controller
        name="field"
        control={control}
        render={({ field, fieldState: { error } }) => {
          return (
            <div>
              <input
                {...field}
                type="number"
                max={Math.max(...Object.values(ACCESS_LEVELS))}
                min={Math.min(...Object.values(ACCESS_LEVELS))}
              />
              {error && <p>{error.message}</p>}
            </div>
          );
        }}
      />
      <button type="submit">remove</button>
    </form>
  );
}
