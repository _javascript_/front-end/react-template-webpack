import React from "react";
import { viewerModel } from "entities/viewer";
import { Level } from "shared/config/access";

import { clsx } from "shared/lib";
import { Typography } from "shared/ui/atoms";


import { bem } from "../../utils";

namespace CurrentLevels {
  export type Props = {
    levels: Level[];
  };
}
function CurrentLevels(props: CurrentLevels.Props) {
  const { levels } = props;
  const classes = clsx(bem("current-levels"));

  return <Typography className={classes}>{levels.join(" | ")}</Typography>;
}

function CurrentLevelsContainer() {
  const levels = viewerModel.selectors.useAccess();
  return <CurrentLevels levels={levels} />;
}

function CurrentLevelSectionComponent() {
  const classes = clsx(
    bem("section"),
    bem("section", { "current-levels": true }),
  );

  return (
    <div className={classes}>
      <Typography>Current access levels</Typography>
      <CurrentLevelsContainer />
    </div>
  );
}

const CurrentLevelSection = React.memo(CurrentLevelSectionComponent);

export { CurrentLevelSection };
