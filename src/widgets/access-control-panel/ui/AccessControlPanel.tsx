import React from "react";
import ReactDOM from "react-dom";

import { clsx } from "shared/lib/clsx";
import { bem } from "../utils";

import {
  CurrentLevelSection,
  AddLevelForm,
  RemoveLevelForm,
} from "./components";

import "./AccessControlPanel.css";

function AccessControlPanelComponent() {
  const classes = clsx(bem());

  return (
    <div className={classes}>
      <CurrentLevelSection />
      <AddLevelForm />
      <RemoveLevelForm />
    </div>
  );
}

const AccessControlPanel = React.memo(AccessControlPanelComponent);

function AccessControlPanelPortalComponent() {
  const classes = clsx(bem("portal"));

  return ReactDOM.createPortal(
    <div className={classes}>
      <AccessControlPanel />
    </div>,
    document.body,
  );
}
const AccessControlPanelPortal = React.memo(AccessControlPanelPortalComponent);

export { AccessControlPanel, AccessControlPanelPortal };
