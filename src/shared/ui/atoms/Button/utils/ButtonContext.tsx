import React, { useContext } from "react";

export type ButtonContextValue = {};
const ButtonContext = React.createContext({} as ButtonContextValue);

export const useButtonContext = () => useContext(ButtonContext);

export type ButtonProviderProps = {
  children: React.ReactNode;
  value: ButtonContextValue;
};
export function ButtonProvider(props: ButtonProviderProps) {
  const { children, value } = props;
  return (
    <ButtonContext.Provider value={value}>{children}</ButtonContext.Provider>
  );
}
