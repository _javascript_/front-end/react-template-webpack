import { uiBem } from "shared/ui/prefix";

export const BUTTON_VARIANTS = {
  TEXT: "text",
  CONTAINED: "contained",
  OUTLINED: "outlined",
} as const;
export const VARIANTS_MAPPING = {} as const;
export const bem = uiBem("button");
