import React, { ButtonHTMLAttributes } from "react";
import { clsx } from "shared/lib/clsx";

import { BUTTON_VARIANTS, bem } from "./config";

export namespace Button {
  type ButtonVariants = typeof BUTTON_VARIANTS;
  export type Variant = ButtonVariants[keyof ButtonVariants];
  export type Props = {
    className?: string;
    variant?: Variant;
    children?: React.ReactNode;
    onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    type?: ButtonHTMLAttributes<HTMLButtonElement>["type"];
  };
}

function Button(props: Button.Props) {
  const {
    variant = "contained",
    className,
    children,
    onClick,
    type = "button",
  } = props;
  const classes = clsx(bem(), bem({ variant }), className);
  return (
    <button onClick={onClick} className={classes} type={type}>
      {children}
    </button>
  );
}

export { Button };
