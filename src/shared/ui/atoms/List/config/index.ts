import { uiBem } from "shared/ui/prefix";

export const LIST_VARIANTS = {} as const;
export const LIST_ITEM_VARIANTS = {} as const;

export const VARIANTS_MAPPING = {} as const;

export const bem = uiBem("list");
