import React from "react";
import { clsx } from "shared/lib/clsx";

import { LIST_VARIANTS, LIST_ITEM_VARIANTS, bem } from "./config";

namespace List {
  type ListVariants = typeof LIST_VARIANTS;
  export type Props = {
    className?: string;
    variant?: Variant;
    children?: React.ReactNode;
  };
  export type Variant = ListVariants[keyof ListVariants];

  type ListItemVariants = typeof LIST_ITEM_VARIANTS;
  export type ItemVariant = ListItemVariants[keyof ListItemVariants];
  export type ItemProps = {
    className?: string;
    variant?: ItemVariant;
    children?: React.ReactNode;
    primaryText?: React.ReactNode;
    secondaryText?: React.ReactNode;
  };
}

function Item(props: List.ItemProps) {
  const { variant, className, children } = props;

  const classes = clsx(bem("item"), bem("item", { variant }), className);

  return <li className={classes}>{children}</li>;
}

function List(props: List.Props) {
  const { variant, className, children } = props;

  const classes = clsx(bem(), bem({ variant }), className);

  return <ul className={classes}>{children}</ul>;
}

List.Item = Item;

export { List };
