import React, { useContext } from "react";

export type ListContextValue = {};
const ListContext = React.createContext({} as ListContextValue);

export const useListContext = () => useContext(ListContext);

export type ListProviderProps = {
  children: React.ReactNode;
  value: ListContextValue;
};
export function ListProvider(props: ListProviderProps) {
  const { children, value } = props;
  return <ListContext.Provider value={value}>{children}</ListContext.Provider>;
}
