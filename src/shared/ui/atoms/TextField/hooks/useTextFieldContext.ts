import { useContext } from "react";

import { TextFieldContext } from "../TextFieldContext";

export function useTextFieldContext() {
  return useContext(TextFieldContext);
}
