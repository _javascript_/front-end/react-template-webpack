import React from "react";

export type TextFieldContextValue = {};

export const TextFieldContext = React.createContext({} as TextFieldContextValue);

