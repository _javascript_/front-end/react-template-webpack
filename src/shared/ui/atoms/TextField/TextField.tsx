import React, { HTMLProps } from "react";
import { clsx } from "shared/lib/clsx";

import { TEXT_FIELD_VARIANTS } from "./config";
import { bem } from "./utils";

namespace TextField {
  type TextFieldVariants = typeof TEXT_FIELD_VARIANTS;
  export type Variant = TextFieldVariants[keyof TextFieldVariants];
  export type Props = HTMLProps<HTMLInputElement> & {
    className?: string;
    variant?: Variant;
  };
}

function TextFieldComponent(props: TextField.Props) {
  const { className, ...rest } = props;

  const classesRoot = clsx(bem(), className);

  return <input className={clsx(classesRoot)} {...rest} />;
}

// eslint-disable-next-line @typescript-eslint/no-redeclare
const TextField = React.memo(TextFieldComponent);

export { TextField };
