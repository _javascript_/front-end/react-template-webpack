import React from "react";

import { TextFieldContext, TextFieldContextValue } from "../TextFieldContext";

export type TextFieldProviderProps = {
  children: React.ReactNode;
  value: TextFieldContextValue;
};

export function TextFieldProvider(props: TextFieldProviderProps) {
  const { children, value } = props;
  return (
    <TextFieldContext.Provider value={value}>
      {children}
    </TextFieldContext.Provider>
  );
}
