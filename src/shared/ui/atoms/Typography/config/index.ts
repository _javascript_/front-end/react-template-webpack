import { uiBem } from "shared/ui/prefix";

export const TYPOGRAPHY_VARIANTS = {
  H1: "h1",
  H2: "h2",
  H3: "h3",
  H4: "h4",
  H5: "h5",
  SUBTITLE1: "subtitle1",
  SUBTITLE2: "subtitle2",
  BODY1: "body1",
  BODY2: "body2",
  CAPTION: "caption",
  BUTTON: "button",
} as const;

export const VARIANTS_MAPPING = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  subtitle1: "h6",
  subtitle2: "h6",
  body1: "p",
  body2: "p",
  caption: "p",
  button: "p",
} as const;

export const bem = uiBem("typography");
