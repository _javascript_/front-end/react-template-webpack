import React from "react";
import { clsx } from "shared/lib/clsx";

import { TYPOGRAPHY_VARIANTS, VARIANTS_MAPPING, bem } from "./config";

export namespace Typography {
  type TypographyVariants = typeof TYPOGRAPHY_VARIANTS;
  export type Variant = TypographyVariants[keyof TypographyVariants];
  export type Props = {
    className?: string;
    variant?: Variant;
    children?: React.ReactNode;
  };
}

export function Typography(props: Typography.Props) {
  const { variant = "body1", className, ...restProps } = props;

  const classes = clsx(bem(), bem({ variant }), className);

  return React.createElement(VARIANTS_MAPPING[variant], {
    className: classes,
    ...restProps,
  });
}
