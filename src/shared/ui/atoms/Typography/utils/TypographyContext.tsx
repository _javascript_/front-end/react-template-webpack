import React, { useContext } from "react";

export type TypographyContextValue = {};
const TypographyContext = React.createContext({} as TypographyContextValue);

export const useTypographyContext = () => useContext(TypographyContext);

export type TypographyProviderProps = {
  children: React.ReactNode;
  value: TypographyContextValue;
};
export function TypographyProvider(props: TypographyProviderProps) {
  const { children, value } = props;
  return (
    <TypographyContext.Provider value={value}>
      {children}
    </TypographyContext.Provider>
  );
}
