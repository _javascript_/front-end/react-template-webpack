import React, { ReactNode } from "react";
import { Helmet } from "react-helmet-async";

export type PageProps = {
  children: ReactNode;
  helmet?: {
    title?: string;
    description?: string;
  };
};

export function Page({ children, helmet }: PageProps) {
  return (
    <>
      <Helmet>
        {helmet?.title && <title>{helmet.title}</title>}
        {helmet?.description && (
          <meta name="description" content={helmet.description} />
        )}
      </Helmet>
      {children}
    </>
  );
}
