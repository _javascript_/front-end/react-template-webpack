import React, { useContext } from "react";

export type LinkContextValue = {};
const LinkContext = React.createContext({} as LinkContextValue);

export const useLinkContext = () => useContext(LinkContext);

export type LinkProviderProps = {
  children: React.ReactNode;
  value: LinkContextValue;
};
export function LinkProvider(props: LinkProviderProps) {
  const { children, value } = props;
  return <LinkContext.Provider value={value}>{children}</LinkContext.Provider>;
}
