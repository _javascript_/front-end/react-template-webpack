import { clsx } from "shared/lib";

import { bem } from "../config";
import { Link } from "../types";

export function useLink(props: Link.Props) {
  const { variant, className, selected } = props;

  // const isSelected =
  //   selected &&
  //   useMatch({
  //     path: to,
  //     end: false,
  //   });

  const classes = clsx(
    bem(),
    { [bem({ variant })]: variant },
    { [bem({ selected })]: selected },
    className,
  );

  return { classes };
}
