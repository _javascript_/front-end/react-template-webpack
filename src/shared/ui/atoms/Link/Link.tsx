import React from "react";
import { Link as BrowserLink } from "react-router-dom";

import { Link as Namespace } from "./types";

import { useLink } from "./utils";

export function Link(props: Namespace.Props) {
  const { children, to } = props;
  const { classes } = useLink(props);
  return (
    <BrowserLink to={to} className={classes}>
      {children}
    </BrowserLink>
  );
}
