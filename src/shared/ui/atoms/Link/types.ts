import { LINK_VARIANTS } from "./config";

export namespace Link {
  type LinkVariants = typeof LINK_VARIANTS;
  export type Variant = LinkVariants[keyof LinkVariants];
  export type Props = {
    className?: string;
    variant?: Variant;
    children?: React.ReactNode;
    to: string;
    selected?: boolean;
  };
}
