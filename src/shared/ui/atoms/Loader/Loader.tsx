import { CircularProgress } from "@mui/material";
import React from "react";
import { clsx } from "shared/lib/clsx";

import { LdsFacebook } from "./components";
import { LOADER_VARIANTS, bem } from "./config";
import { LoaderProvider, useLoader } from "./utils";


export namespace Loader {
  type LoaderVariants = typeof LOADER_VARIANTS;
  export type Variant = LoaderVariants[keyof LoaderVariants];
  export type Props = {
    className?: string;
    variant?: Variant;
  };
}

function View(props: Loader.Props) {
  const { className, variant = "lds-facebook" } = props;

  const classesRoot = clsx(bem(), className);
  return (
    <div className={clsx(classesRoot)}>
      {variant === LOADER_VARIANTS.LDS_FACEBOOK ? (
        <LdsFacebook />
      ) : (
        variant === LOADER_VARIANTS.CIRCUL && <CircularProgress />
      )}
    </div>
  );
}

const MemoView = React.memo(View);

export function Loader(props: Loader.Props) {
  useLoader();

  return (
    <LoaderProvider value={{}}>
      <MemoView {...props} />
    </LoaderProvider>
  );
}
