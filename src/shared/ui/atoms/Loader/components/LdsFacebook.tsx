import React from "react";

import "./LdsFacebook.css";

export function LdsFacebook() {
  return (
    <div className="lds-facebook">
      <div />
      <div />
      <div />
    </div>
  );
}
