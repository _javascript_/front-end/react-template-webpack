import { uiBem } from "shared/ui/prefix";

export const LOADER_VARIANTS = {
  LDS_FACEBOOK: "lds-facebook",
  CIRCUL: "circul",
} as const;
export const bem = uiBem("loader");
