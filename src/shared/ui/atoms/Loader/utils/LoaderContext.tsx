import React, { useContext } from "react";

export type LoaderContextValue = {};
const LoaderContext = React.createContext({} as LoaderContextValue);

export const useLoaderContext = () => useContext(LoaderContext);

export type LoaderProviderProps = {
  children: React.ReactNode;
  value: LoaderContextValue;
};
export function LoaderProvider(props: LoaderProviderProps) {
  const { children, value } = props;
  return (
    <LoaderContext.Provider value={value}>{children}</LoaderContext.Provider>
  );
}
