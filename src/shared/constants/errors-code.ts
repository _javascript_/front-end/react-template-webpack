export const ERRORS_CODE = {
  404: "NOT FOUND",
  403: "FORBIDDEN",
} as const;
export type ErrorsCode = typeof ERRORS_CODE;
export type Code = keyof ErrorsCode;
export type Error = ErrorsCode[Code];
