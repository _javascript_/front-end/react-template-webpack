export const IDLE = "idle";
export const LOADING = "loading";
export const SUCCEEDED = "succeeded";
export const ERROR = "error";

export const REQUEST_STATUSES = {
  IDLE,
  LOADING,
  SUCCEEDED,
  ERROR,
} as const;

export type RequestStatuses = typeof REQUEST_STATUSES;
export type RequestStatusKeys = keyof RequestStatuses;
export type RequestStatus = RequestStatuses[RequestStatusKeys];
