import axios, { AxiosError } from "axios";

export function initErrorInterceptor(cb?: (error: AxiosError) => void) {
  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error instanceof AxiosError) {
        if (cb) cb(error);
      }
      return Promise.reject(error);
    },
  );
}
