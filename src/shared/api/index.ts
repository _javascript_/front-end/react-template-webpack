import { AuthToken } from "shared/utils/auth-token";

import { VksFsoClient } from "./vksfso";

export const vksfso = new VksFsoClient({ TOKEN: async () => AuthToken.get() });
