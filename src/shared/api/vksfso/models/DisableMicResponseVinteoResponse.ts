/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DisableMicResponse } from './DisableMicResponse';

/**
 * Модель ответа Vinteo API.
 */
export type DisableMicResponseVinteoResponse = {
    /**
     * Статус.
     */
    status?: string;
    data?: DisableMicResponse;
};

