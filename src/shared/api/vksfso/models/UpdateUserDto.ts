/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateUpdateUserPhoneNumberDto } from './CreateUpdateUserPhoneNumberDto';

/**
 * Модель обновления пользователя.
 */
export type UpdateUserDto = {
    /**
     * Имя пользователя.
     */
    name?: string;
    /**
     * email.
     */
    email?: string;
    /**
     * Идентификатор роли.
     */
    roleId?: number;
    /**
     * Список идентификаторов организаций.
     */
    organizationIds?: Array<number>;
    /**
     * Список идентификаторов подгрупп.
     */
    subgroupIds?: Array<number>;
    /**
     * Аватар.
     */
    avatar?: string | null;
    /**
     * Список номеров телефонов пользователя.
     */
    phoneNumbers?: Array<CreateUpdateUserPhoneNumberDto>;
};

