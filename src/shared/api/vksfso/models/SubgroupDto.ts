/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель подгруппы.
 */
export type SubgroupDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
};

