/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ConferenceSettings } from './ConferenceSettings';

/**
 * Модель конференции ответа на запрос создания/обновления/получения конференции.
 */
export type ConferenceInfoResponse = {
    /**
     * Номер конференции
     */
    number?: number;
    /**
     * Описание
     */
    description?: string;
    /**
     * Статус режима лектора
     */
    lecturerMode?: boolean;
    /**
     * Статус записи конференции
     */
    recording?: boolean;
    /**
     * Статус трансляций
     */
    webcast?: boolean;
    /**
     * Статус презентации
     */
    presentation?: boolean;
    settings?: ConferenceSettings;
    /**
     * Статус конференции
     */
    active?: boolean;
    /**
     * Временная
     */
    temporary?: boolean;
};

