/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель удаления нескольких сущностей.
 */
export type MultipleRemoveDto = {
    /**
     * Список идентификаторов.
     */
    ids?: Array<number>;
};

