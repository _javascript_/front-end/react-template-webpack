/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель добавления пользователей в подгруппу.
 */
export type AddUsersToSubgroupDto = {
    /**
     * Идентификатор подгруппы.
     */
    subgroupId?: number;
    /**
     * Список идентификаторов пользователей.
     */
    userIds?: Array<number>;
};

