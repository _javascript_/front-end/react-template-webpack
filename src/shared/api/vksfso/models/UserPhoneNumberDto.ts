/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель номера телефона пользователя.
 */
export type UserPhoneNumberDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Номер телефона.
     */
    phoneNumber?: string;
    /**
     * Флаг - является ли данный номер телефона основным.
     */
    primary?: boolean;
};

