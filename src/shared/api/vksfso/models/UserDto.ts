/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель пользователя.
 */
export type UserDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Список организаций.
     */
    organizations?: Array<string>;
    /**
     * Список подгрупп.
     */
    subgroups?: Array<string>;
    /**
     * Имя пользователя.
     */
    name?: string;
    /**
     * Роль.
     */
    role?: string;
    /**
     * Статус.
     */
    status?: string;
};

