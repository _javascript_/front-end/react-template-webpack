/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Общая модель POST запросов контроллера участников.
 */
export type ParticipantsPostGeneral = {
    /**
     * Номер конференции
     */
    conference?: number;
    /**
     * Массив участников
     */
    participants?: Array<string>;
};

