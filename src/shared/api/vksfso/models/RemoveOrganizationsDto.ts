/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель удаления нескольких организаций.
 */
export type RemoveOrganizationsDto = {
    /**
     * Список идентификаторов.
     */
    ids?: Array<number>;
};

