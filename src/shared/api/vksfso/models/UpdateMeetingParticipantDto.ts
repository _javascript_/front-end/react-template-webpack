/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель для обновления участников конференции.
 */
export type UpdateMeetingParticipantDto = {
    /**
     * Идентификатор пользователя.
     */
    userId?: number;
    /**
     * Является ли пользователь модератором.
     */
    isModer?: boolean;
};

