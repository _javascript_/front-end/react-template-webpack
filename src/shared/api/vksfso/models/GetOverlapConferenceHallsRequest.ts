/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BaseScheduleDto } from './BaseScheduleDto';

/**
 * Модель запроса получения конференц-залов имеющих пересечения по расписанию.
 */
export type GetOverlapConferenceHallsRequest = {
    /**
     * Список идентификаторов конференц-залов.
     */
    conferenceHallIds?: Array<number>;
    /**
     * Идентификатор конференции.
     */
    conferenceId?: number | null;
    schedule?: BaseScheduleDto;
};

