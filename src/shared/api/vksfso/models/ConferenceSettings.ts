/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AnonymousConnections } from './AnonymousConnections';
import type { Watermark } from './Watermark';

/**
 * Модель настроек конференции.
 */
export type ConferenceSettings = {
    /**
     * Мозаика
     */
    mosaic?: string;
    /**
     * ПИН-код конференции
     */
    pin?: number;
    /**
     * Не выключать конференцию
     */
    shutdown?: boolean;
    /**
     * Перезванивать самостоятельно отключившимся
     */
    reCall?: boolean;
    /**
     * При подключении микрофон выключен
     */
    micOff?: boolean;
    /**
     * Вызвать участников при входе модератора
     */
    callPartsOnModerSignIn?: boolean;
    /**
     * Отключить участников при выходе модератора
     */
    disconnectPartsOnModerSignOut?: boolean;
    watermark?: Watermark;
    /**
     * Список модераторов
     */
    moderators?: Array<string>;
    /**
     * Постоянное декодирование видео потока
     */
    alwaysDecode?: boolean;
    /**
     * Период определения лектора (ms)
     */
    lecturerDelay?: number;
    /**
     * Длительность вызова участника (сек.)
     */
    callDuration?: number;
    /**
     * Кол-во попыток вызова
     */
    callAttempt?: number;
    /**
     * Интервал между вызовами (сек.)
     */
    callInterval?: number;
    /**
     * Включить автоматические сообщения
     */
    displayNotification?: boolean;
    /**
     * Презентация без мозаики
     */
    presentationWithoutMosaic?: boolean;
    /**
     * Открытая конференция
     */
    openConference?: boolean;
    /**
     * Отображать собственное видео
     */
    showSelfVideo?: boolean;
    /**
     * Отображать watermark
     */
    displayWatermark?: boolean;
    /**
     * Показывать скриншоты после подключения участников
     */
    enableScreenshots?: boolean;
    /**
     * Титры
     */
    titers?: boolean;
    /**
     * Персональная раскладка лектора
     */
    customLayoutLecturer?: boolean;
    /**
     * Отображать участников без видео
     */
    partsWithoutVideo?: boolean;
    /**
     * Автоматические назначенные панели
     */
    autoLayout?: boolean;
    /**
     * Приоритет режима лектора
     */
    lecturerPriority?: boolean;
    /**
     * Дублирование панелей
     */
    duplicatePanels?: boolean;
    /**
     * Период прокрутки
     */
    scrollInterval?: number;
    /**
     * Не занимать позицию лектор назначенным панелям
     */
    canBecomeLecturer?: boolean;
    /**
     * Способ отображения участников в раскладке
     */
    cropVideo?: number;
    /**
     * Рамки участников
     */
    bordersParticipants?: boolean;
    /**
     * Индикация записи конференции
     */
    recordingIndication?: boolean;
    /**
     * Отображать фон watermark
     */
    displayWatermarkBackground?: boolean;
    /**
     * Конфиденциально
     */
    secretly?: number;
    /**
     * Соотношение сторон раскладки 4:3
     */
    fourToThree?: boolean;
    /**
     * Разрешить участникам отправлять презентацию
     */
    allowSendH239?: boolean;
    /**
     * Громкость тихих аудио каналов
     */
    quiet?: number;
    /**
     * Максимальное количество участников
     */
    maxParticipants?: number;
    /**
     * Автоматическое приглушение аудиоканалов: [0] - выключено; [1] - по группе; [2] - по переводчику
     */
    autoQuietChannels?: number;
    /**
     * Режим оптимизации клиента [0, 1, 2]
     */
    clientOptimisationMode?: number;
    anonymousConnections?: AnonymousConnections;
};

