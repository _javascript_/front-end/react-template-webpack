/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AddAccountResponse } from './AddAccountResponse';

/**
 * Модель ответа Vinteo API.
 */
export type AddAccountResponseVinteoResponse = {
    /**
     * Статус.
     */
    status?: string;
    data?: AddAccountResponse;
};

