/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель подгруппы с количеством пользователей.
 */
export type SubgroupUserCountDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Количество пользователей.
     */
    usersCount?: number;
};

