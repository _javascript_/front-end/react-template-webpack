/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель создания/обновления номера телефона пользователя.
 */
export type CreateUpdateUserPhoneNumberDto = {
    /**
     * Номер телефона.
     */
    phoneNumber?: string;
    /**
     * Флаг - является ли данный номер телефона основным.
     */
    primary?: boolean;
};

