/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель роли.
 */
export type RoleDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    name?: string;
    /**
     * Локализованное наименование.
     */
    localizedName?: string;
};

