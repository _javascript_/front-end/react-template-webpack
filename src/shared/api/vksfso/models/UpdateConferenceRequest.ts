/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель запроса изменения параметров конференции.
 */
export type UpdateConferenceRequest = {
    /**
     * Описание
     * Ограничения: 1-120
     */
    description?: string;
    /**
     * Пин-код конференции
     * Unique conference PIN code, default value is set random 7 digits, can be empty, cannot contain digit "3"
     * example: 987654
     * Ограничения: 1000-9999
     * maxLength: 15; minLength: 1; pattern: ^[12456789][012456789]{0,14}$; example: 987654
     */
    pin?: number | null;
    /**
     * Список модераторов
     */
    moderators?: Array<string> | null;
    /**
     * Не выключать конференцию
     * По умолчанию: true
     */
    shutdown?: boolean | null;
    /**
     * Перезванивать самостоятельно отключившимся
     * По умолчанию: false
     */
    reCall?: boolean | null;
    /**
     * При подключении микрофон выключен
     * По умолчанию: false
     */
    micOff?: boolean | null;
    /**
     * Вызвать участников при входе модератора
     * По умолчанию: false
     */
    callPartsOnModerSignIn?: boolean | null;
    /**
     * Отключить участников при выходе модератора
     * По умолчанию: false
     */
    disconnectPartsOnModerSignOut?: boolean | null;
    /**
     * Мозаика
     * Допустимые значения: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 31, 34, 35, 36, 37, 38, 40, 49, 50, 52, 105, 106, 108
     */
    mosaic?: string | null;
    /**
     * Период определения лектора (ms)
     * По умолчанию: 3000
     * Ограничения: 20-10000
     */
    lecturerDelay?: number | null;
    /**
     * Длительность вызова участника (сек.)
     * По умолчанию: 30
     * Ограничения: 1-60
     */
    callDuration?: number | null;
    /**
     * Кол-во попыток вызова
     * По умолчанию: 3
     * Ограничения: 1-9
     */
    callAttempt?: number | null;
    /**
     * Интервал между вызовами (сек.)
     * По умолчанию: 60
     * Ограничения: 1-180
     */
    callInterval?: number | null;
    /**
     * Включить автоматические сообщения
     * По умолчанию: false
     */
    displayNotification?: boolean | null;
    /**
     * Презентация без мозаики
     * По умолчанию: false
     */
    presentationWithoutMosaic?: boolean | null;
    /**
     * Открытая конференция
     * По умолчанию: false
     */
    openConference?: boolean | null;
    /**
     * Отображать собственное видео
     * По умолчанию: true
     */
    showSelfVideo?: boolean | null;
    /**
     * Отображать watermark
     * По умолчанию: true
     */
    displayWatermark?: boolean | null;
    /**
     * Показывать скриншоты после подключения участников
     * По умолчанию: true
     */
    enableScreenshots?: boolean | null;
    /**
     * Титры
     * По умолчанию: false
     */
    titers?: boolean | null;
    /**
     * Персональная раскладка лектора
     * По умолчанию: false
     */
    customLayoutLecturer?: boolean | null;
    /**
     * Отображать участников без видео
     * По умолчанию: true
     */
    partsWithoutVideo?: boolean | null;
    /**
     * Автоматические назначенные панели
     * По умолчанию: false
     */
    autoLayout?: boolean | null;
    /**
     * Приоритет режима лектора
     * По умолчанию: false
     */
    lecturerPriority?: boolean | null;
    /**
     * Дублирование панелей
     * По умолчанию: false
     */
    duplicatePanels?: boolean | null;
    /**
     * Период прокрутки
     * По умолчанию: 30
     * Ограничения: 10-300
     */
    scrollInterval?: number | null;
    /**
     * Не занимать позицию лектор назначенным панелям
     * По умолчанию: false
     */
    canBecomeLecturer?: boolean | null;
    /**
     * Способ отображения участников в раскладке: 0 - вписывать, 1 - обрезать, 2 - растягивать
     * По умолчанию: 0
     */
    cropVideo?: number | null;
    /**
     * Рамки участников
     * По умолчанию: false
     */
    bordersParticipants?: boolean | null;
    /**
     * Индикация записи конференции
     * По умолчанию: false
     */
    recordingIndication?: boolean | null;
    /**
     * Отображать фон watermark
     * По умолчанию: false
     */
    displayWatermarkBackground?: boolean | null;
    /**
     * Конфиденциально
     * По умолчанию: 0
     * Допустимые значения: 0, 1, 2
     */
    secretly?: number | null;
    /**
     * Соотношение сторон раскладки 4:3
     * По умолчанию: false
     */
    fourToThree?: boolean | null;
    /**
     * Разрешить участникам отправлять презентацию
     * По умолчанию: true
     */
    allowSendH239?: boolean | null;
    /**
     * Громкость тихих аудио каналов
     * По умолчанию: 0
     * Ограничения: 0-10
     */
    quiet?: number | null;
    /**
     * Максимальное количество участников
     * По умолчанию: 0
     * Ограничения: 0-2147483647
     */
    maxParticipants?: number | null;
    /**
     * Длительность показа (сек.)
     * По умолчанию: 0
     * Минимум: 0
     */
    watermarkDuration?: number | null;
    /**
     * Размер шрифта watermark
     * По умолчанию: 70
     * Ограничения: 1-90
     */
    watermarkSize?: number | null;
    /**
     * Цвет текста
     * По умолчанию: #ffffff
     */
    fontColor?: string | null;
    /**
     * Цвет фона
     * По умолчанию: #000000
     */
    fontBackground?: string | null;
    /**
     * Прозрачность фона
     * По умолчанию: 0
     * Ограничения: 0-100
     */
    fontBackgroundOpacity?: number | null;
    /**
     * Тип фона
     * По умолчанию: 0
     * Допустимые значения: 0, 1
     */
    fontBackgroundType?: number | null;
    /**
     * Положение watermark
     * По умолчанию: 1
     * Допустимые значения: 0, 1
     */
    watermarkPosition?: number | null;
    /**
     * Автоматическое приглушение аудиоканалов: [0] - выключено; [1] - по группе; [2] - по переводчику
     * По умолчанию: 0
     * Допустимые значения: 0, 1, 2
     */
    autoQuietChannels?: number | null;
    /**
     * Client resolution/bitrate optimisation mode: [0] - disabled; [1] - minimize visual loss; [2] - enabled (full)
     * По умолчанию: 2
     * Допустимые значения: 0, 1, 2
     */
    clientOptimisationMode?: number | null;
    /**
     * Битрейт по умолчанию для анонимных абонентов конференции, зависит от дистриба
     * По умолчанию: 1536
     * Допустимые значения: 256, 320, 384, 512, 768, 1024, 1536, 2048, 2560, 3072, 3584, 4096, 4608, 5120, 5632, 6144
     */
    anonymousBandwidth?: number | null;
    /**
     * Разрешение видео по умолчанию для анонимных абонентов конференции, зависит от дистриба
     * По умолчанию: 720p
     * Допустимые значения: CIF, 4CIF, 640x360, 720p, FULLHD
     */
    anonymousResolution?: string | null;
    /**
     * Фпс по умолчанию для анонимных абонентов конференции, зависит от дистриба
     * По умолчанию: 25
     * Допустимые значения: 15, 25, 30, 45, 60
     */
    anonymousFPS?: number | null;
};

