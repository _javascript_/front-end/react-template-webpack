/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель для постраничного списка конференций.
 */
export type PagedMeetingDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Дата и время создания конференции
     */
    createdAt?: string;
    /**
     * Идентификатор конференции в системе ВКС.
     */
    vksMeetingId?: number;
    /**
     * Наименование (тема).
     */
    title?: string;
    /**
     * Наименование.
     */
    organizationTitle?: string;
    /**
     * Статус
     * Запланирована
     * Текущая
     * Завершена
     * Удалена
     */
    status?: string | null;
};

