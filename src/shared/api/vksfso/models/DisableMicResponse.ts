/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ParticipantsPostGeneral } from './ParticipantsPostGeneral';

/**
 * Модель ответа на запрос выключения микрофонов участников.
 */
export type DisableMicResponse = {
    disableMic?: ParticipantsPostGeneral;
};

