/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель обновления организации.
 */
export type UpdateOrganizationDto = {
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Максимальное кол-во онлайн мероприятий.
     */
    maxMeetings?: number | null;
    /**
     * Максимальное кол-во участников мероприятий.
     */
    maxParticipants?: number | null;
    /**
     * Список идентификаторов администраторов.
     */
    administratorIds?: Array<number>;
};

