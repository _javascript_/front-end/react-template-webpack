/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель создания организации.
 */
export type CreateOrganizationDto = {
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Максимальное кол-во онлайн мероприятий.
     */
    maxMeetings?: number | null;
    /**
     * Максимальное кол-во участников мероприятий.
     */
    maxParticipants?: number | null;
    /**
     * Список идентификаторов администраторов.
     */
    administratorIds?: Array<number>;
};

