/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель обновления конференц-зала.
 */
export type UpdateConferenceHallDto = {
    /**
     * Идентификатор организации.
     */
    organizationId?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Адрес.
     */
    address?: string | null;
    /**
     * Параметры вызова SIP/H323 устройства.
     */
    callParams?: string | null;
};

