/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель идентификатора и наименования сущности.
 */
export type Int64IdTitleDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
};

