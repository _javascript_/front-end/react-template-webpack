/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Упрощенная модель пользователя.
 */
export type SimpleUserDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Имя пользователя.
     */
    name?: string;
};

