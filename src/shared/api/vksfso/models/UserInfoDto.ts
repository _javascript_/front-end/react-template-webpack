/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель информации о пользователе.
 */
export type UserInfoDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Имя пользователя.
     */
    name?: string;
    /**
     * Роль.
     */
    role?: string;
    /**
     * Локализованное наименование роли.
     */
    localizedRoleName?: string;
    /**
     * Аватар.
     */
    avatar?: string | null;
};

