/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель настроек по умолчанию для анонимных абонентов конфы.
 */
export type AnonymousConnections = {
    /**
     * Пропускная способность канала
     */
    bandwidth?: number;
    /**
     * FPS
     */
    fps?: number;
    /**
     * Разрешение видео
     */
    resolution?: string;
};

