/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель участника для добавления в конференцию для пагинированного списка.
 */
export type PagedParticipantDto = {
    /**
     * <inheritdoc cref="T:VKS.FSO.Core.Entities.Entity`1" />
     */
    id?: number;
    /**
     * ФИО.
     */
    fio?: string;
    /**
     * Номер телефона.
     */
    phoneNumber?: string | null;
    /**
     * Роль.
     */
    localizedRoleName?: string;
};

