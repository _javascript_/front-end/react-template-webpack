/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель обновления подгруппы.
 */
export type UpdateSubgroupDto = {
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Идентификаторы пользователей
     */
    userIds?: Array<number>;
};

