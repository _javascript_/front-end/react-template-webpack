/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SubgroupDto } from './SubgroupDto';

/**
 * Модель организации.
 */
export type OrganizationDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Список подгрупп.
     */
    subgroups?: Array<SubgroupDto>;
};

