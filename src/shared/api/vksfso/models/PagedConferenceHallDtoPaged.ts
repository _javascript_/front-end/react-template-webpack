/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PagedConferenceHallDto } from './PagedConferenceHallDto';
import type { PageMeta } from './PageMeta';

/**
 * Модель постраничного списка сущностей.
 */
export type PagedConferenceHallDtoPaged = {
    pageMeta?: PageMeta;
    /**
     * Возвращает или задает список элементов постраничного списка.
     */
    elements?: Array<PagedConferenceHallDto>;
};

