/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DetailedMeetingsApplicationUserDto } from './DetailedMeetingsApplicationUserDto';

/**
 * Сущность участника конференции.
 */
export type DetailedMeetingParticipantDto = {
    user?: DetailedMeetingsApplicationUserDto;
    /**
     * Является ли пользователь модератором.
     */
    isModer?: boolean;
    /**
     * Статус приглашения: "На рассмотрении", "Принято", "Отклонено"
     */
    inviteStatus?: string;
};

