/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель идентификатора и наименования сущности.
 */
export type StringIdTitleDto = {
    /**
     * Идентификатор.
     */
    id?: string;
    /**
     * Наименование.
     */
    title?: string;
};

