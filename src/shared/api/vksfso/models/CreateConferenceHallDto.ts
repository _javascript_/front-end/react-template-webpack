/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель создания конференц-зала.
 */
export type CreateConferenceHallDto = {
    /**
     * Идентификатор организации.
     */
    organizationId?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Адрес.
     */
    address?: string | null;
    /**
     * Параметры вызова SIP/H323 устройства.
     */
    callParams?: string | null;
};

