/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AccountResponse } from './AccountResponse';

/**
 * Модель ответа на запрос создания абонента.
 */
export type AddAccountResponse = {
    account?: AccountResponse;
};

