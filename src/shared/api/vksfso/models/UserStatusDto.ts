/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель статуса пользователя.
 */
export type UserStatusDto = {
    /**
     * Тэг.
     */
    tag?: string;
    /**
     * Наименование.
     */
    title?: string;
};

