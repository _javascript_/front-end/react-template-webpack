/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserPhoneNumberDto } from './UserPhoneNumberDto';

/**
 * Модель пользователя.
 */
export type SingleUserDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Имя.
     */
    name?: string;
    /**
     * Email.
     */
    email?: string;
    /**
     * Список идентификаторов организаций.
     */
    organizationIds?: Array<number>;
    /**
     * Список идентификаторов подгрупп.
     */
    subgroupIds?: Array<number>;
    /**
     * Идентификатор роли.
     */
    roleId?: number;
    /**
     * Список номеров телефонов пользователей.
     */
    phoneNumbers?: Array<UserPhoneNumberDto> | null;
};

