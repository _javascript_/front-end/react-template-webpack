/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ParticipantsPostGeneral } from './ParticipantsPostGeneral';

/**
 * Модель ответа на запрос включения микрофонов участников.
 */
export type EnableMicResponse = {
    enableMic?: ParticipantsPostGeneral;
};

