/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель параметров Watermark
 */
export type Watermark = {
    /**
     * Длительность показа (сек.)
     */
    watermarkDuration?: number;
    /**
     * Размер шрифта watermark
     */
    watermarkSize?: number;
    /**
     * Цвет текста
     */
    fontColor?: string;
    /**
     * Цвет фона
     */
    fontBackground?: string;
    /**
     * Прозрачность фона
     */
    fontBackgroundOpacity?: number;
    /**
     * Тип фона
     */
    fontBackgroundType?: number;
    /**
     * Положение watermark
     */
    watermarkPosition?: number;
};

