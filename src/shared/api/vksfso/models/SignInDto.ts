/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель авторизации.
 */
export type SignInDto = {
    /**
     * Возвращает или задает email.
     */
    email?: string;
    /**
     * Возвращает или задает пароль.
     */
    password?: string;
};

