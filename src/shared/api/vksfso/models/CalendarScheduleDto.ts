/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель календарного элемента расписания.
 */
export type CalendarScheduleDto = {
    /**
     * Идентификатор элемента расписания.
     */
    id?: number;
    /**
     * Идентификатор конференции.
     */
    meetingId?: number;
    /**
     * Тема.
     */
    title?: string;
    /**
     * Место.
     */
    place?: string | null;
    /**
     * Ссылка.
     */
    link?: string | null;
    /**
     * Дата и время начала.
     */
    start?: string;
    /**
     * Дата и время окончания.
     */
    end?: string;
    /**
     * ФИО инициатора.
     */
    initiatorFio?: string;
};

