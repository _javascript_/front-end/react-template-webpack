/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AccountGroup } from './AccountGroup';
import type { AccountSettings } from './AccountSettings';
import type { AccountStatus } from './AccountStatus';

/**
 * Модель ответа на запрос параметров абонента.
 */
export type AccountResponse = {
    /**
     * Тип абонента
     * Допустимые значения: SIP, H323, WS, Loop
     */
    type?: string;
    /**
     * Номер абонента
     */
    number?: string;
    /**
     * Имя или описание абонента
     */
    description?: string;
    /**
     * Пароль
     */
    password?: string;
    /**
     * Email
     */
    email?: string;
    group?: AccountGroup;
    settings?: AccountSettings;
    status?: AccountStatus;
};

