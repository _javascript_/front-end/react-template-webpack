/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель удаления пользователей из подгруппы.
 */
export type RemoveUsersFromSubgroupDto = {
    /**
     * Идентификатор подгруппы.
     */
    subgroupId?: number;
    /**
     * Список идентификаторов пользователей.
     */
    userIds?: Array<number>;
};

