/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PageMeta } from './PageMeta';
import type { UserDto } from './UserDto';

/**
 * Модель постраничного списка сущностей.
 */
export type UserDtoPaged = {
    pageMeta?: PageMeta;
    /**
     * Возвращает или задает список элементов постраничного списка.
     */
    elements?: Array<UserDto>;
};

