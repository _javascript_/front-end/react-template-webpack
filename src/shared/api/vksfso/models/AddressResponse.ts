/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IceServer } from './IceServer';

/**
 * Модель ответа на запрос получения информации о сервере.
 */
export type AddressResponse = {
    /**
     * ip адрес.
     */
    ip?: string;
    /**
     * ip удаленного сервера.
     */
    remoteAddress?: string;
    unified?: boolean;
    /**
     * Порт stun.
     */
    stunPort?: number;
    /**
     * Список ice серверов.
     */
    iceServers?: Array<IceServer>;
};

