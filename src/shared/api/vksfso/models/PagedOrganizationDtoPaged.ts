/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PagedOrganizationDto } from './PagedOrganizationDto';
import type { PageMeta } from './PageMeta';

/**
 * Модель постраничного списка сущностей.
 */
export type PagedOrganizationDtoPaged = {
    pageMeta?: PageMeta;
    /**
     * Возвращает или задает список элементов постраничного списка.
     */
    elements?: Array<PagedOrganizationDto>;
};

