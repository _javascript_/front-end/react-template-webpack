/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель ice сервера.
 */
export type IceServer = {
    /**
     * Адрес.
     */
    urls?: string;
};

