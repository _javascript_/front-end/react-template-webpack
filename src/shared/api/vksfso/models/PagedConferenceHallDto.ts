/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель конференц-зала для списка с пагинацией.
 */
export type PagedConferenceHallDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    organizationTitle?: string;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Адрес.
     */
    address?: string | null;
    /**
     * Параметры вызова SIP/H323 устройства.
     */
    callParams?: string | null;
};

