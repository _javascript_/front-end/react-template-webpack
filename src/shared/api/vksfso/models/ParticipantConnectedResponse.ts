/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель ответа на запрос добавления записи о подключившемся участнике.
 */
export type ParticipantConnectedResponse = {
    /**
     * <inheritdoc cref="T:VKS.FSO.Core.Entities.Entity`1" />
     */
    id?: number;
    /**
     * Имя участника.
     */
    name?: string | null;
    /**
     * Является ли участник модератором.
     */
    isModer?: boolean;
};

