/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель для создания расписания конференции.
 */
export type CreateScheduleDto = {
    /**
     * Тип периодичности.
     * "once", "daily", "weekly", "monthly"
     * Одноразовое/ежедневное/еженедельное/ежемесячное
     */
    type?: string;
    /**
     * Время начала конференции.
     */
    startTime?: string;
    /**
     * Время окончания конференции.
     */
    endTime?: string;
    /**
     * Интервал повторения в днях
     * Повторять каждый N день
     */
    everyDayNumber?: number | null;
    /**
     * Флаг повторения каждый рабочий день
     * Интеграции с производственным или иным календарем/расписанием сотрудников не планируется, за рабочие дни принимаем понедельник - пятница
     */
    everyWorkingDay?: boolean | null;
    /**
     * Интервал повторения в неделях
     * Повторять каждую N неделю
     */
    everyWeekNumber?: number | null;
    /**
     * Еженедельное повторение в указанные дни
     * Значение определяется по битовой маске
     * воскресенье = 1, понедельник = 2, вторник = 4, среда = 8, четверг = 16, пятница = 32, суббота = 64
     * например, если выбраны суббота + пятница, двоичное значение будет 1100000, в бд будет десятичное значение 96
     */
    everyWeekDays?: number | null;
    /**
     * Повторять указанного числа каждого выбранного месяца
     */
    dayOfMonth?: number | null;
    /**
     * Интервал повторения в месяцах
     * Повторять каждого N месяца
     */
    everyMonthNumber?: number | null;
    /**
     * Повторять в (1, 2, 3, 4 или последний) monthly_day_number каждого выбранного месяца
     * На текущем этапе реализация не планируется!
     */
    monthlyDay?: string | null;
    /**
     * Повторять в monthly_day (день, рабочий день, выходной день, воскресенье, понедельник, вторник, среда, четверг, пятница, суббота) каждого выбранного месяца
     */
    monthlyDayNumber?: string | null;
    /**
     * Дата начала интервала для построения календарных элементов расписания
     */
    startDate?: string;
    /**
     * Дата окончания интервала для построения календарных элементов расписания
     */
    endDate?: string | null;
    /**
     * Количество повторений, после которых планирование заканчивается
     * Указывается что-то одно - или дата окончания интервала или количество повторений
     */
    repeatCount?: number | null;
};

