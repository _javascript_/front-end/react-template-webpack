/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SubgroupUserCountDto } from './SubgroupUserCountDto';

/**
 * Модель организации для пагинированного списка.
 */
export type PagedOrganizationDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Дата и время создания.
     */
    createdAt?: string;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Список подгрупп.
     */
    subgroups?: Array<SubgroupUserCountDto>;
    /**
     * Количество пользователей.
     */
    usersCount?: number;
    /**
     * Список администраторов.
     */
    administrators?: Array<string>;
};

