/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель ответа на запрос списка записей.
 */
export type GetRecordsResponseRecords = {
    /**
     * ID записи
     */
    id?: number;
    /**
     * Номер конференции
     */
    conference?: string;
    /**
     * Описание конференции
     */
    description?: string;
    /**
     * Дата
     */
    date?: string;
    /**
     * Длительность записи (сек.)
     */
    duration?: string;
    /**
     * Ссылка на запись
     */
    link?: string;
};

