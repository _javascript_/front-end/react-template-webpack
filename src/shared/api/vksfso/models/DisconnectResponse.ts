/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ParticipantsPostGeneral } from './ParticipantsPostGeneral';

/**
 * Модель ответа на запрос отключения участников.
 */
export type DisconnectResponse = {
    disconnect?: ParticipantsPostGeneral;
};

