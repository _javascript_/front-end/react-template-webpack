/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DisconnectResponse } from './DisconnectResponse';

/**
 * Модель ответа Vinteo API.
 */
export type DisconnectResponseVinteoResponse = {
    /**
     * Статус.
     */
    status?: string;
    data?: DisconnectResponse;
};

