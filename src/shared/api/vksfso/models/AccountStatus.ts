/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель статуса абонента
 */
export type AccountStatus = {
    /**
     * Зарегистрирован
     */
    connected?: boolean;
    /**
     * Адрес с которого зарегистрирован абонент
     */
    address?: string;
};

