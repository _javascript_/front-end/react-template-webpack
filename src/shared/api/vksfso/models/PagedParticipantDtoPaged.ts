/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PagedParticipantDto } from './PagedParticipantDto';
import type { PageMeta } from './PageMeta';

/**
 * Модель постраничного списка сущностей.
 */
export type PagedParticipantDtoPaged = {
    pageMeta?: PageMeta;
    /**
     * Возвращает или задает список элементов постраничного списка.
     */
    elements?: Array<PagedParticipantDto>;
};

