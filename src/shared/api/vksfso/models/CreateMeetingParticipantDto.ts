/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель для создания участников конференции.
 */
export type CreateMeetingParticipantDto = {
    /**
     * Идентификатор пользователя.
     */
    userId?: number;
    /**
     * Является ли пользователь модератором.
     */
    isModer?: boolean;
};

