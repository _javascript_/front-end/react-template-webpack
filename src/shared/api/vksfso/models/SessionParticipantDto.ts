/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель участника сессии.
 */
export type SessionParticipantDto = {
    /**
     * <inheritdoc cref="T:VKS.FSO.Core.Entities.Entity`1" />
     */
    id?: number | null;
    /**
     * Имя участника.
     */
    name?: string;
    /**
     * Является ли участник модератором.
     */
    isModer?: boolean;
    /**
     * Аватар пользователя.
     */
    avatar?: string | null;
    /**
     * Участник запросил слово
     */
    requestWord?: boolean;
    /**
     * Статус подключения
     */
    isOnline?: boolean;
    /**
     * Участник был отключен
     */
    isDisconnected?: boolean;
    /**
     * Уровень громкости микрофона
     */
    volume?: number | null;
    /**
     * Статус микрофона
     */
    mic?: boolean;
    /**
     * Статус камеры
     */
    cam?: boolean;
    /**
     * Признак выключен ли микрофон модератором.
     */
    muteByModer?: boolean;
    /**
     * Признак выключена ли камера модератором.
     */
    camDisabledByModer?: boolean;
};

