/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Настройки абонента
 */
export type AccountSettings = {
    /**
     * Ширина канала kb/s
     * Допустимые значения: 256, 320, 384, 512, 768, 1024, 1536, 2048, 2560, 3072, 3584, 4096, 4608, 5120, 5632, 6144
     */
    bandwidth?: number;
    /**
     * Разрешение для P2P звонков
     * Допустимые значения: CIF, 4CIF, 640x360, 720p, FULLHD
     */
    resolutionP2P?: string;
    /**
     * Опции NAT для SIP и H.323 абонентов
     * Допустимые значения: no, yes, never
     */
    behindNat?: string;
    /**
     * IP адрес (Только для SIP и H.323 абонентов), если vpn=true, то поле ip обязательно для заполнения
     */
    ip?: string;
    /**
     * Порт (Только для SIP и H.323 абонентов)
     */
    port?: number;
    /**
     * Транспорт (Только для SIP абонентов)
     * Допустимые значения: TCP, TLS, UDP
     */
    transport?: string;
    /**
     * Insecure (Только для SIP абонентов)
     * Допустимые значения: port, invite, very
     */
    insecure?: string;
    /**
     * Qualify (Только для SIP абонентов)
     */
    qualify?: boolean;
    /**
     * Список кодеков (Не используется для Loop абонентов)
     */
    codecs?: Array<string>;
    /**
     * H.264 High Profile (Только для SIP и H.323 абонентов)
     */
    h264HighProfile?: boolean;
    /**
     * Проключать медиа потоки до ответа (Только для SIP и H.323 абонентов)
     */
    mediaStreams?: boolean;
    /**
     * Режим DTMF (Только для SIP и H.323 абонентов)
     * Допустимые значения: rfc2833, auto, inband, info
     */
    dtmf?: string;
    /**
     * Тип BFCP (Только для SIP абонентов)
     * Допустимые значения: TCP, UDP, NONE
     */
    bfcpType?: string;
    /**
     * Media Encryption (Только для SIP абонентов)
     */
    mediaEncryption?: boolean;
    /**
     * Skype4B (Только для SIP абонентов)
     */
    skype4B?: boolean;
    /**
     * H4601 (Только для H.323 абонентов)
     */
    h4601?: boolean;
    /**
     * H.239 (Только для H.323 абонентов)
     */
    h239?: boolean;
    /**
     * H.224 (Только для H.323 абонентов)
     */
    h224?: boolean;
    /**
     * Шифрование канала H.323
     */
    crypto?: boolean;
    /**
     * Разрешение видео абонента в конференции по умолчанию
     * Допустимые значения: 128x72, 256x144, 320x180, 384x216, 512x288, 640x360, 768x432, 896x504, 1024x576, 1152x648,
     * 1408x792, 1536x864, 1664x936, 1792x1008, QCIF, CIF, VGA, 4SIF, 4CIF, 480p, 576p, PAL, SVGA, XGA, 720p, 16CIF, FULLHD, UHD, 7680×4320
     */
    defaultResolution?: string;
    /**
     * Не показывать пользователя в раскладке
     */
    ignore?: boolean;
    /**
     * Разрешить абоненту WS совершать групповые звонки
     */
    allowGroupCall?: boolean;
    /**
     * Включить для абонента функции переводчика
     */
    interpreter?: boolean;
    /**
     * Канал аудио по умолчанию для абонента WS с функциями переводчика
     */
    privateAudioChannel?: number;
    /**
     * Обслуживается шлюзом (Только для Loop абонентов)
     */
    serveGateway?: string;
    /**
     * Фильтрация RTP (Только для SIP и H.323 абонентов)
     */
    protectRTP?: boolean;
    /**
     * Use Panasonic RTP (Только для SIP и H.323 абонентов)
     */
    panasonicFEC?: boolean;
};

