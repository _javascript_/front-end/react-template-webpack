/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PagedMeetingDto } from './PagedMeetingDto';
import type { PageMeta } from './PageMeta';

/**
 * Модель постраничного списка сущностей.
 */
export type PagedMeetingDtoPaged = {
    pageMeta?: PageMeta;
    /**
     * Возвращает или задает список элементов постраничного списка.
     */
    elements?: Array<PagedMeetingDto>;
};

