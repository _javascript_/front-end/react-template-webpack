/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель добавления пользователей в организацию.
 */
export type AddUsersToOrganizationDto = {
    /**
     * Идентификатор организации.
     */
    organizationId?: number;
    /**
     * Идентификатор подгруппы.
     */
    subgroupId?: number | null;
    /**
     * Список идентификаторов пользователей.
     */
    userIds?: Array<number>;
};

