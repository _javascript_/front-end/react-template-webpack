/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель создания подгруппы.
 */
export type CreateSubgroupDto = {
    /**
     * Идентификатор организации.
     */
    organizationId?: number;
    /**
     * Наименование.
     */
    title?: string;
};

