/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Событие vinteo https://api.vinteo.com/swagger-ui/#/Webhooks/post_api_v2_webhooks_subscribers
 */
export type WebhookEvent = {
    /**
     * Уникальный номер события(в текущей реализации целочисленный восходящий номер)
     */
    id?: string;
    /**
     * Время сервера в микросекундах на момент генерации события.
     */
    timestamp?: string;
    /**
     * Типа события
     */
    type?: string;
    /**
     * Информация о событие
     */
    payload?: string;
};

