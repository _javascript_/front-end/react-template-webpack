/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель удаления пользователей.
 */
export type RemoveUsersDto = {
    /**
     * Список идентификаторов пользователей для удаления.
     */
    userIds?: Array<number>;
};

