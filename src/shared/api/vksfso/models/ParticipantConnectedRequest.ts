/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель запроса сохранения информации о подключившемся участнике конференции.
 */
export type ParticipantConnectedRequest = {
    /**
     * Идентификатор сессии.
     */
    sessionId?: number;
    /**
     * Идентификатор участника конференции от сервера Vinteo.
     */
    vksId?: string;
};

