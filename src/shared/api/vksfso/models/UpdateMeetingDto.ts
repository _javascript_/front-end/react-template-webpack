/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UpdateMeetingParticipantDto } from './UpdateMeetingParticipantDto';
import type { UpdateMeetingSettingsDto } from './UpdateMeetingSettingsDto';
import type { UpdateScheduleDto } from './UpdateScheduleDto';

/**
 * Модель для обновления конференции.
 */
export type UpdateMeetingDto = {
    /**
     * Наименование (тема).
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Тип конференции.
     */
    type?: string;
    /**
     * Идентификатор инициатора конференции
     */
    initiatorId?: number;
    meetingSettings?: UpdateMeetingSettingsDto;
    /**
     * Список связей пользователя с конференциями в роли участника.
     */
    meetingParticipants?: Array<UpdateMeetingParticipantDto> | null;
    schedule?: UpdateScheduleDto;
    /**
     * Список идентификаторов конференц-залов.
     */
    conferenceHallIds?: Array<number>;
};

