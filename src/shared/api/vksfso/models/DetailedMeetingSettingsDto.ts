/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель для отображения настроек конференции.
 */
export type DetailedMeetingSettingsDto = {
    /**
     * При подключении участников микрофон выключен.
     */
    micOffOnConnect?: boolean;
    /**
     * Запретить участникам включать микрофон.
     */
    micDisable?: boolean;
    /**
     * При подключении камеры участников отключены.
     */
    camOffOnConnect?: boolean;
    /**
     * Автоматическая регулировка усиления.
     */
    automaticGainControl?: boolean;
    /**
     * Вызвать участников при входе модератора.
     */
    callPartsOnModerSignIn?: boolean;
    /**
     * Отключение участников при выходе модератора.
     */
    disconnectPartsOnModerSignOut?: boolean;
    /**
     * Включение записи конференции.
     */
    record?: boolean;
    /**
     * Раскладка.
     */
    layout?: string;
    /**
     * Разрешить пользователям портала скачивать записи конференций
     */
    allowDownloadRecordings?: boolean;
    /**
     * Включить запись автоматически
     */
    autoRecord?: boolean;
};

