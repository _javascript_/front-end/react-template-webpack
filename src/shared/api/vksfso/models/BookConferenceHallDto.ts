/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель бронирования конференц-зала.
 */
export type BookConferenceHallDto = {
    /**
     * Дата и время начала интервала бронирования конференц-зала.
     */
    start?: string;
    /**
     * Дата и время окончания интервала бронирования конференц-зала.
     */
    end?: string;
    /**
     * Идентификатор конференц-зала.
     */
    conferenceHallId?: number;
    /**
     * Идентификатор руководителя.
     */
    supervisorId?: number;
};

