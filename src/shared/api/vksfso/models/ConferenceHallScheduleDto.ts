/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель элемента расписания конференц-зала.
 */
export type ConferenceHallScheduleDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Возвращает или задает имя пользователя (возможно ФИО).
     */
    supervisor?: string;
    /**
     * Дата и время начала интервала бронирования конференц-зала.
     */
    start?: string;
    /**
     * Дата и время окончания интервала бронирования конференц-зала.
     */
    end?: string;
    /**
     * Наименование.
     */
    organizationTitle?: string;
    /**
     * Адрес.
     */
    address?: string | null;
    /**
     * Флаг, есть ли привязка к ВКС конференции или это очное бронирование.
     */
    hasConference?: boolean;
};

