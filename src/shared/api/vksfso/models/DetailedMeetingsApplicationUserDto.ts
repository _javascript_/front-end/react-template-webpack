/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель для отображения участников конференции.
 */
export type DetailedMeetingsApplicationUserDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Возвращает или задает имя пользователя (возможно ФИО).
     */
    name?: string;
    /**
     * Номер мобильного телефона
     */
    phoneNumber?: string;
    /**
     * Возвращает или задает роли.
     */
    role?: string | null;
    /**
     * Список связей пользователей с организациями.
     */
    organization?: string | null;
};

