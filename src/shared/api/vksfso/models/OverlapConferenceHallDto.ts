/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { OverlapConferenceHallScheduleDto } from './OverlapConferenceHallScheduleDto';

/**
 * Модель конференц-залов имеющих пересечения по расписанию.
 */
export type OverlapConferenceHallDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Адрес.
     */
    address?: string | null;
    /**
     * Список элементов расписания имеющих пересечение по времени проведения.
     */
    overlapConferenceHallSchedules?: Array<OverlapConferenceHallScheduleDto>;
};

