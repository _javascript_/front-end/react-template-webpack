/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель информации о сессии.
 */
export type SessionInfoDto = {
    /**
     * Идентификатор сессии.
     */
    id?: number;
    /**
     * Наименование конференции.
     */
    title?: string;
    /**
     * Признак ведется ли сейчас запись.
     */
    record?: boolean;
};

