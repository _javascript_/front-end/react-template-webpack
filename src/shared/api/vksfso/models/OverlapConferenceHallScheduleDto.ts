/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель пересечения элемента расписания конференц-зала.
 */
export type OverlapConferenceHallScheduleDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Дата и время начала интервала бронирования конференц-зала.
     */
    startAt?: string;
    /**
     * Дата и время окончания интервала бронирования конференц-зала.
     */
    endAt?: string;
};

