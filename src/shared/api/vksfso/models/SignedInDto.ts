/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель данных авторизованного пользователя.
 */
export type SignedInDto = {
    /**
     * Возвращает или задает идентификатор пользователя.
     */
    id?: number;
    /**
     * Возвращает или задает токен доступа.
     */
    accessToken?: string;
    /**
     * Возвращает или задает email.
     */
    email?: string;
    /**
     * Возвращает или задает имя пользователя (возможно ФИО).
     */
    name?: string;
    /**
     * Возвращает или задает роль пользователя.
     */
    role?: string;
};

