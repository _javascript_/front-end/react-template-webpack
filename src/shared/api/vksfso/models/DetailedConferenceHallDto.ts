/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Детальная модель конференц-зала.
 */
export type DetailedConferenceHallDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Идентификатор организации.
     */
    organizationId?: number;
    /**
     * Наименование.
     */
    organizationTitle?: string;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Адрес.
     */
    address?: string | null;
    /**
     * Параметры вызова SIP/H323 устройства.
     */
    callParams?: string | null;
};

