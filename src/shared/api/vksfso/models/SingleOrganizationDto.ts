/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SimpleUserDto } from './SimpleUserDto';
import type { SubgroupUserCountDto } from './SubgroupUserCountDto';

/**
 * Модель организации.
 */
export type SingleOrganizationDto = {
    /**
     * Идентификатор.
     */
    id?: number;
    /**
     * Наименование.
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Максимальное кол-во онлайн мероприятий.
     */
    maxMeetings?: number | null;
    /**
     * Максимальное кол-во участников мероприятий.
     */
    maxParticipants?: number | null;
    /**
     * Список подгрупп.
     */
    subgroups?: Array<SubgroupUserCountDto>;
    /**
     * Список администраторов.
     */
    administrators?: Array<SimpleUserDto>;
};

