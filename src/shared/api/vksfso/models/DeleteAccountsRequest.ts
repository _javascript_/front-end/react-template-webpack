/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель запроса удаления абонентов Vinteo.
 */
export type DeleteAccountsRequest = {
    /**
     * Список номеров абонентов.
     */
    numbers?: Array<string>;
};

