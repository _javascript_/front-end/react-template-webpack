/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CreateMeetingParticipantDto } from './CreateMeetingParticipantDto';
import type { CreateMeetingSettingsDto } from './CreateMeetingSettingsDto';
import type { CreateScheduleDto } from './CreateScheduleDto';

/**
 * Модель для создания конференции.
 */
export type CreateMeetingDto = {
    /**
     * Наименование (тема).
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Тип конференции.
     */
    type?: string;
    /**
     * Идентификатор инициатора конференции
     */
    initiatorId?: number;
    meetingSettings?: CreateMeetingSettingsDto;
    /**
     * Список связей пользователя с конференциями в роли участника.
     */
    meetingParticipants?: Array<CreateMeetingParticipantDto> | null;
    schedule?: CreateScheduleDto;
    /**
     * Список идентификаторов конференц-залов.
     */
    conferenceHallIds?: Array<number>;
};

