/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EnableMicResponse } from './EnableMicResponse';

/**
 * Модель ответа Vinteo API.
 */
export type EnableMicResponseVinteoResponse = {
    /**
     * Статус.
     */
    status?: string;
    data?: EnableMicResponse;
};

