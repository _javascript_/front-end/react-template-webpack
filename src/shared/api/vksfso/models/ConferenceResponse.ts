/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ConferenceInfoResponse } from './ConferenceInfoResponse';

/**
 * Модель ответа на запрос создания/обновления/получения конференции.
 */
export type ConferenceResponse = {
    conference?: ConferenceInfoResponse;
};

