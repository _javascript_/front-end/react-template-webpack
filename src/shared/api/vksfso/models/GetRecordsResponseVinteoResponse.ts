/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { GetRecordsResponse } from './GetRecordsResponse';

/**
 * Модель ответа Vinteo API.
 */
export type GetRecordsResponseVinteoResponse = {
    /**
     * Статус.
     */
    status?: string;
    data?: GetRecordsResponse;
};

