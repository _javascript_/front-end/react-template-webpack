/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DetailedConferenceHallDto } from './DetailedConferenceHallDto';
import type { DetailedMeetingParticipantDto } from './DetailedMeetingParticipantDto';
import type { DetailedMeetingSettingsDto } from './DetailedMeetingSettingsDto';
import type { DetailedScheduleDto } from './DetailedScheduleDto';

/**
 * Модель для отображения конференции.
 */
export type DetailedMeetingDto = {
    /**
     * Возвращает или задает идентификатор.
     */
    id?: number;
    /**
     * Наименование (тема).
     */
    title?: string;
    /**
     * Описание.
     */
    description?: string | null;
    /**
     * Тип конференции.
     */
    type?: string;
    /**
     * Ссылка гостевого доступа.
     */
    guestLink?: string | null;
    /**
     * Ссылка подключения к конференции.
     */
    connectLink?: string | null;
    /**
     * Статус
     * Запланирована
     * Текущая
     * Завершена
     * Удалена
     */
    status?: string | null;
    meetingSettings?: DetailedMeetingSettingsDto;
    /**
     * Список связей пользователя с конференциями в роли участника.
     */
    meetingParticipants?: Array<DetailedMeetingParticipantDto> | null;
    schedule?: DetailedScheduleDto;
    /**
     * Список конференц-залов.
     */
    conferenceHalls?: Array<DetailedConferenceHallDto> | null;
};

