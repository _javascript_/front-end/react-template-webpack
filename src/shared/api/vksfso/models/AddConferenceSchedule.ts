/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель планировки при создании конференции.
 */
export type AddConferenceSchedule = {
    /**
     * Дата (timestamp) (значение должно быть больше текущего времени)
     */
    date?: number;
    /**
     * Длительность конференции (мин.)
     * Ограничения: минимум 10
     */
    duration?: number;
    /**
     * Вызвать всех участников после начала конференции
     * По умолчанию: false
     */
    call?: boolean | null;
    /**
     * Отправить email уведомления участникам конференции
     * По умолчанию: false
     */
    notification?: boolean | null;
    /**
     * Email организатора
     */
    organizer?: string | null;
    /**
     * Автоматически включить запись конференции
     * По умолчанию: false
     */
    record?: boolean | null;
};

