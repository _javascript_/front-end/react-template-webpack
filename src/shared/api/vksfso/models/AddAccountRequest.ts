/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель запроса создания нового абонента.
 */
export type AddAccountRequest = {
    /**
     * Номер абонента
     * Ограничения: 1-32
     */
    number?: string | null;
    /**
     * Тип абонента
     * По умолчанию: SIP
     * Допустимые значения: SIP, H323, WS, Loop
     */
    type?: string | null;
    /**
     * Список конференций, в которые нужно добавить абонента
     */
    conferences?: Array<string> | null;
    /**
     * Имя или описание абонента
     * Ограничения: 1-64
     */
    description?: string | null;
    /**
     * Пароль
     * Ограничения: 3-16
     */
    password?: string | null;
    /**
     * Идентификатор группы, куда надо добавить аккаунт или null
     */
    group?: string | null;
    /**
     * Email
     */
    email?: string | null;
    /**
     * Ширина канала kb/s
     * По умолчанию: 1536
     * Допустимые значения: 256, 320, 384, 512, 768, 1024, 1536, 2048, 2560, 3072, 3584, 4096, 4608, 5120, 5632, 6144
     */
    bandwidth?: number | null;
    /**
     * Разрешение для P2P звонков
     * По умолчанию: 720p
     * Допустимые значения: CIF, 4CIF, 640x360, 720p, FULLHD
     */
    resolutionP2P?: string | null;
    /**
     * Проключать медиа потоки до ответа (Только для SIP и H.323 абонентов)
     * По умолчанию: false
     */
    mediaStreams?: boolean | null;
    /**
     * Опции NAT для SIP и H.323 абонентов
     * По умолчанию: no
     * Допустимые значения: no, yes, never
     */
    behindNat?: string | null;
    /**
     * H.264 High Profile (Только для SIP и H.323 абонентов)
     * По умолчанию: false
     */
    h264HighProfile?: boolean | null;
    /**
     * Qualify (Только для SIP абонентов)
     * По умолчанию: true
     */
    qualify?: boolean | null;
    /**
     * Media Encryption (Только для SIP абонентов)
     * По умолчанию: false
     */
    mediaEncryption?: boolean | null;
    /**
     * Skype4B (Только для SIP абонентов)
     * По умолчанию: false
     */
    skype4B?: boolean | null;
    /**
     * H4601 (Только для H.323 абонентов)
     * По умолчанию: true
     */
    h4601?: boolean | null;
    /**
     * IP адрес (Только для SIP и H.323 абонентов), если vpn=true, то поле ip обязательно для заполнения
     * По умолчанию: dynamic
     */
    ip?: string | null;
    /**
     * Порт (Только для SIP и H.323 абонентов)
     * По умолчанию: 5060
     */
    port?: number | null;
    /**
     * Транспорт (Только для SIP абонентов)
     * По умолчанию: UDP
     * Допустимые значения: TCP, TLS, UDP
     */
    transport?: string | null;
    /**
     * Insecure (Только для SIP абонентов)
     * По умолчанию: port
     * Допустимые значения: port, invite, very
     */
    insecure?: string | null;
    /**
     * Тип BFCP (Только для SIP абонентов)
     * По умолчанию: UDP
     * Допустимые значения: TCP, UDP, NONE
     */
    bfcpType?: string | null;
    /**
     * Режим DTMF (Только для SIP и H.323 абонентов)
     * По умолчанию: rfc2833
     * Допустимые значения: rfc2833, auto, inband, info
     */
    dtmf?: string | null;
    /**
     * Обслуживается шлюзом (Только для Loop абонентов)
     * По умолчанию: auto
     */
    serveGateway?: string | null;
    /**
     * Настройки видеопотока (Только для SIP и Loop абонентов)
     */
    stream?: string | null;
    /**
     * Список кодеков (Не используется для Loop абонентов)
     * По умолчанию: ['g7221','ulaw','h265','h264']
     */
    codecs?: Array<string> | null;
    /**
     * H.239 (Только для H.323 абонентов)
     * По умолчанию: true
     */
    h239?: boolean | null;
    /**
     * H.224 (Только для H.323 абонентов)
     * По умолчанию: false
     */
    h224?: boolean | null;
    /**
     * Шифрование канала H.323
     * По умолчанию: true
     */
    crypto?: boolean | null;
    /**
     * Разрешение видео абонента в конференции по умолчанию
     * По умолчанию: 720p
     * Допустимые значения: 128x72, 256x144, 320x180, 384x216, 512x288, 640x360, 768x432, 896x504, 1024x576, 1152x648,
     * 1408x792, 1536x864, 1664x936, 1792x1008, QCIF, CIF, VGA, 4SIF, 4CIF, 480p, 576p, PAL, SVGA, XGA, 720p, 16CIF, FULLHD, UHD, 7680×4320
     */
    defaultResolution?: string | null;
    /**
     * Не показывать пользователя в раскладке
     * По умолчанию: false
     */
    ignore?: boolean | null;
    /**
     * Разрешить абоненту WS совершать групповые звонки
     * По умолчанию: false
     */
    allowGroupCall?: boolean | null;
    /**
     * Включить для абонента функции переводчика
     * По умолчанию: false
     */
    interpreter?: boolean | null;
    /**
     * Фильтрация RTP (Только для SIP и H.323 абонентов)
     * По умолчанию: true
     */
    protectRTP?: boolean | null;
    /**
     * Use Panasonic RTP (Только для SIP и H.323 абонентов)
     * По умолчанию: true
     */
    panasonicFEC?: boolean | null;
};

