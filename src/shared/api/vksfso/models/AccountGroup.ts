/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель группы абонента.
 */
export type AccountGroup = {
    /**
     * Идентификатор
     */
    id?: number;
    /**
     * Название
     */
    name?: string;
    /**
     * LDAP
     */
    ldap?: boolean;
};

