/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель блокировки пользователей.
 */
export type BlockUsersDto = {
    /**
     * Список идентификаторов пользователей для блокировки.
     */
    userIds?: Array<number>;
};

