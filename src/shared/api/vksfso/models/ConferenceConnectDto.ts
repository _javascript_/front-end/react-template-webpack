/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AddressResponse } from './AddressResponse';

/**
 * Модель с информацией о подключении к конференции.
 */
export type ConferenceConnectDto = {
    /**
     * Идентификатор сессии.
     */
    sessionId?: number;
    /**
     * Идентификатор конференции с сервера Vinteo.
     */
    vksConferenceId?: number;
    serverInfo?: AddressResponse;
};

