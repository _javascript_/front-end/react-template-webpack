/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Модель удаления нескольких подгрупп.
 */
export type RemoveSubgroupsDto = {
    /**
     * Список идентификаторов.
     */
    ids?: Array<number>;
};

