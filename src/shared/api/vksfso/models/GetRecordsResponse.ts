/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { GetRecordsResponseRecords } from './GetRecordsResponseRecords';

/**
 * Модель ответа на запрос списка записей.
 */
export type GetRecordsResponse = {
    /**
     * Общее количество элементов.
     */
    totalCount?: number;
    /**
     * Список записей.
     */
    records?: Array<GetRecordsResponseRecords>;
};

