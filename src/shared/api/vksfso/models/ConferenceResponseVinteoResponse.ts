/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ConferenceResponse } from './ConferenceResponse';

/**
 * Модель ответа Vinteo API.
 */
export type ConferenceResponseVinteoResponse = {
    /**
     * Статус.
     */
    status?: string;
    data?: ConferenceResponse;
};

