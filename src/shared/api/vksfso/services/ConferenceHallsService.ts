/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateConferenceHallDto } from '../models/CreateConferenceHallDto';
import type { DetailedConferenceHallDto } from '../models/DetailedConferenceHallDto';
import type { Int64IdTitleDto } from '../models/Int64IdTitleDto';
import type { MultipleRemoveDto } from '../models/MultipleRemoveDto';
import type { PagedConferenceHallDtoPaged } from '../models/PagedConferenceHallDtoPaged';
import type { UpdateConferenceHallDto } from '../models/UpdateConferenceHallDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ConferenceHallsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Создает конференц-зал.
     * @returns DetailedConferenceHallDto Success
     * @throws ApiError
     */
    public postApiConferenceHalls({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.CreateConferenceHallDto" path="/summary/node()" />
         */
        requestBody?: CreateConferenceHallDto,
    }): CancelablePromise<DetailedConferenceHallDto> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/conference-halls',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает экземпляр конференц-зала.
     * @returns DetailedConferenceHallDto Success
     * @throws ApiError
     */
    public getApiConferenceHalls({
        id,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
    }): CancelablePromise<DetailedConferenceHallDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/conference-halls/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Обновляет экземпляр конференц-зала.
     * @returns any Success
     * @throws ApiError
     */
    public putApiConferenceHalls({
        id,
        requestBody,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.UpdateConferenceHallDto" path="/summary/node()" />
         */
        requestBody?: UpdateConferenceHallDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/conference-halls/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет конференц-зал.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiConferenceHalls({
        id,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/conference-halls/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает пагинированный список конференц-залов.
     * @returns PagedConferenceHallDtoPaged Success
     * @throws ApiError
     */
    public getApiConferenceHallsPaged({
        organizationIds,
        sortBy,
        page,
        perPage,
    }: {
        /**
         * Возвращает или задает список идентификаторов организаций для фильтрации.
         */
        organizationIds?: Array<number>,
        /**
         * Возвращает или задает сортировку.
         */
        sortBy?: Array<string>,
        /**
         * Возвращает или задает номер страницы.
         */
        page?: number,
        /**
         * Возвращает или задает количество элементов на странице.
         */
        perPage?: number,
    }): CancelablePromise<PagedConferenceHallDtoPaged> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/conference-halls/paged',
            query: {
                'organizationIds': organizationIds,
                'sortBy': sortBy,
                'page': page,
                'perPage': perPage,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет несколько конференц-залов.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiConferenceHallsMultiple({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.MultipleRemoveDto" path="/summary/node()" />
         */
        requestBody?: MultipleRemoveDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/conference-halls/multiple',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список адресов конференц-залов.
     * @returns string Success
     * @throws ApiError
     */
    public getApiConferenceHallsAddresses({
        organizationId,
    }: {
        /**
         * Идентификатор организации.
         */
        organizationId?: number,
    }): CancelablePromise<Array<string>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/conference-halls/addresses',
            query: {
                'organizationId': organizationId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает упрощенный список конференц-залов.
     * @returns Int64IdTitleDto Success
     * @throws ApiError
     */
    public getApiConferenceHallsSimple({
        organizationId,
        address,
    }: {
        /**
         * Идентификатор организации.
         */
        organizationId?: number,
        /**
         * Адрес конференц-зала.
         */
        address?: string,
    }): CancelablePromise<Array<Int64IdTitleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/conference-halls/simple',
            query: {
                'organizationId': organizationId,
                'address': address,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
