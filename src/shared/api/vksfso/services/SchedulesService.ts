/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CalendarScheduleDto } from '../models/CalendarScheduleDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class SchedulesService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Возвращает список календарных элементов расписания за интервал.
     * @returns CalendarScheduleDto Success
     * @throws ApiError
     */
    public getApiSchedulesCalendar({
        start,
        end,
    }: {
        /**
         * Начало интервала.
         */
        start?: string,
        /**
         * Окончание интервала.
         */
        end?: string,
    }): CancelablePromise<Array<CalendarScheduleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/schedules/calendar',
            query: {
                'start': start,
                'end': end,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
