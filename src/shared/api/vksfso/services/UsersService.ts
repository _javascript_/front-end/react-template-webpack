/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddUsersToOrganizationDto } from '../models/AddUsersToOrganizationDto';
import type { AddUsersToSubgroupDto } from '../models/AddUsersToSubgroupDto';
import type { BlockUsersDto } from '../models/BlockUsersDto';
import type { CreateUserDto } from '../models/CreateUserDto';
import type { RemoveUsersDto } from '../models/RemoveUsersDto';
import type { RoleDto } from '../models/RoleDto';
import type { SingleUserDto } from '../models/SingleUserDto';
import type { UpdateUserDto } from '../models/UpdateUserDto';
import type { UserDtoPaged } from '../models/UserDtoPaged';
import type { UserInfoDto } from '../models/UserInfoDto';
import type { UserStatusDto } from '../models/UserStatusDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class UsersService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Создает пользователя.
     * @returns SingleUserDto Success
     * @throws ApiError
     */
    public postApiUsers({
        requestBody,
    }: {
        /**
         * Данные для создания пользователя.
         */
        requestBody?: CreateUserDto,
    }): CancelablePromise<SingleUserDto> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/users',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает пагинированный список пользователей.
     * @returns UserDtoPaged Success
     * @throws ApiError
     */
    public getApiUsers({
        organizationId,
        search,
        status,
        sortBy,
        page,
        perPage,
    }: {
        /**
         * Возвращает или задает идентификатор организации.
         */
        organizationId?: number,
        /**
         * Возвращает или задает комплексный поиск по имени или идентификатору.
         */
        search?: string,
        /**
         * Возвращает или задает статус.
         */
        status?: string,
        /**
         * Возвращает или задает сортировку.
         */
        sortBy?: Array<string>,
        /**
         * Возвращает или задает номер страницы.
         */
        page?: number,
        /**
         * Возвращает или задает количество элементов на странице.
         */
        perPage?: number,
    }): CancelablePromise<UserDtoPaged> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/users',
            query: {
                'organizationId': organizationId,
                'search': search,
                'status': status,
                'sortBy': sortBy,
                'page': page,
                'perPage': perPage,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Обновляет пользователя.
     * @returns any Success
     * @throws ApiError
     */
    public putApiUsers({
        id,
        requestBody,
    }: {
        /**
         * Идентификатор пользователя.
         */
        id: number,
        /**
         * Данные для обновления пользователя.
         */
        requestBody?: UpdateUserDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/users/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает пользователя по идентификатору.
     * @returns SingleUserDto Success
     * @throws ApiError
     */
    public getApiUsers1({
        id,
    }: {
        /**
         * Идентификатор пользователя.
         */
        id: number,
    }): CancelablePromise<Array<SingleUserDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/users/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список статусов пользователей.
     * @returns UserStatusDto Success
     * @throws ApiError
     */
    public getApiUsersStatuses(): CancelablePromise<Array<UserStatusDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/users/statuses',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Добавляет пользователей в организацию.
     * @returns any Success
     * @throws ApiError
     */
    public postApiUsersAddToOrganization({
        requestBody,
    }: {
        /**
         * Данные для добавления пользователей в организацию.
         */
        requestBody?: AddUsersToOrganizationDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/users/add-to-organization',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Добавляет пользователей в подгруппу.
     * @returns any Success
     * @throws ApiError
     */
    public postApiUsersAddToSubgroup({
        requestBody,
    }: {
        /**
         * Данные для добавления пользователей в подгруппу.
         */
        requestBody?: AddUsersToSubgroupDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/users/add-to-subgroup',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет пользователей.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiUsersRemove({
        requestBody,
    }: {
        /**
         * Данные для удаления пользователей.
         */
        requestBody?: RemoveUsersDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/users/remove',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Блокирует пользователей.
     * @returns any Success
     * @throws ApiError
     */
    public putApiUsersBlock({
        requestBody,
    }: {
        /**
         * Данные для блокировки пользователей.
         */
        requestBody?: BlockUsersDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/users/block',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список ролей.
     * @returns RoleDto Success
     * @throws ApiError
     */
    public getApiUsersRoles(): CancelablePromise<Array<RoleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/users/roles',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает информацию о текущем пользователе.
     * @returns UserInfoDto Success
     * @throws ApiError
     */
    public getApiUsersUserInfo(): CancelablePromise<UserInfoDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/users/user-info',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

}
