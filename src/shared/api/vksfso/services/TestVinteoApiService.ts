/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddAccountRequest } from '../models/AddAccountRequest';
import type { AddAccountResponseVinteoResponse } from '../models/AddAccountResponseVinteoResponse';
import type { AddConferenceRequest } from '../models/AddConferenceRequest';
import type { ConferenceResponseVinteoResponse } from '../models/ConferenceResponseVinteoResponse';
import type { DeleteAccountsRequest } from '../models/DeleteAccountsRequest';
import type { DisableMicResponseVinteoResponse } from '../models/DisableMicResponseVinteoResponse';
import type { DisconnectResponseVinteoResponse } from '../models/DisconnectResponseVinteoResponse';
import type { EnableMicResponseVinteoResponse } from '../models/EnableMicResponseVinteoResponse';
import type { GetRecordsResponseVinteoResponse } from '../models/GetRecordsResponseVinteoResponse';
import type { ParticipantsPostGeneral } from '../models/ParticipantsPostGeneral';
import type { UpdateConferenceRequest } from '../models/UpdateConferenceRequest';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class TestVinteoApiService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns AddAccountResponseVinteoResponse Success
     * @throws ApiError
     */
    public postTestVinteoApiV1Accounts({
        requestBody,
    }: {
        requestBody?: AddAccountRequest,
    }): CancelablePromise<AddAccountResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/test/vinteo/api/v1/accounts',
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns any Success
     * @throws ApiError
     */
    public deleteTestVinteoApiV1Account({
        accountNumber,
    }: {
        accountNumber: string,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/test/vinteo/api/v1/account/{accountNumber}',
            path: {
                'accountNumber': accountNumber,
            },
        });
    }

    /**
     * @returns any Success
     * @throws ApiError
     */
    public postTestVinteoApiV1DeleteAccounts({
        requestBody,
    }: {
        requestBody?: DeleteAccountsRequest,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/test/vinteo/api/v1/delete_accounts',
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns ConferenceResponseVinteoResponse Success
     * @throws ApiError
     */
    public postTestVinteoApiV1Conferences({
        requestBody,
    }: {
        requestBody?: AddConferenceRequest,
    }): CancelablePromise<ConferenceResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/test/vinteo/api/v1/conferences',
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns any Success
     * @throws ApiError
     */
    public deleteTestVinteoApiV1Conference({
        conferenceNumber,
    }: {
        conferenceNumber: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/test/vinteo/api/v1/conference/{conferenceNumber}',
            path: {
                'conferenceNumber': conferenceNumber,
            },
        });
    }

    /**
     * @returns ConferenceResponseVinteoResponse Success
     * @throws ApiError
     */
    public patchTestVinteoApiV1Conference({
        conferenceNumber,
        requestBody,
    }: {
        conferenceNumber: string,
        requestBody?: UpdateConferenceRequest,
    }): CancelablePromise<ConferenceResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/test/vinteo/api/v1/conference/{conferenceNumber}',
            path: {
                'conferenceNumber': conferenceNumber,
            },
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns EnableMicResponseVinteoResponse Success
     * @throws ApiError
     */
    public postTestVinteoApiV1EnableMic({
        requestBody,
    }: {
        requestBody?: ParticipantsPostGeneral,
    }): CancelablePromise<EnableMicResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/test/vinteo/api/v1/enable_mic',
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns DisableMicResponseVinteoResponse Success
     * @throws ApiError
     */
    public postTestVinteoApiV1DisableMic({
        requestBody,
    }: {
        requestBody?: ParticipantsPostGeneral,
    }): CancelablePromise<DisableMicResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/test/vinteo/api/v1/disable_mic',
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns DisconnectResponseVinteoResponse Success
     * @throws ApiError
     */
    public postTestVinteoApiV1Disconnect({
        requestBody,
    }: {
        requestBody?: ParticipantsPostGeneral,
    }): CancelablePromise<DisconnectResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/test/vinteo/api/v1/disconnect',
            body: requestBody,
            mediaType: 'application/json-patch+json',
        });
    }

    /**
     * @returns GetRecordsResponseVinteoResponse Success
     * @throws ApiError
     */
    public getTestVinteoApiV1Records({
        from,
        to,
        sort,
        direction,
        search,
        limit,
        offset,
    }: {
        /**
         * С даты (Формат YYYY-MM-DD)
         */
        from?: string,
        /**
         * По указанную дату (Формат YYYY-MM-DD)
         */
        to?: string,
        /**
         * Сортировка по полю
         * По умолчанию: conference
         * Допустимые значения: "conference", "date"
         */
        sort?: string,
        /**
         * Направление сортировки
         * По умолчанию: asc
         * Допустимые значения: "asc", "desc"
         */
        direction?: string,
        /**
         * Поиск
         */
        search?: string,
        /**
         * Кол-во записей
         * По умолчанию: 20
         */
        limit?: number,
        /**
         * Отступ
         * По умолчанию: 0
         */
        offset?: number,
    }): CancelablePromise<GetRecordsResponseVinteoResponse> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/test/vinteo/api/v1/records',
            query: {
                'From': from,
                'To': to,
                'Sort': sort,
                'Direction': direction,
                'Search': search,
                'Limit': limit,
                'Offset': offset,
            },
        });
    }

    /**
     * @returns any Success
     * @throws ApiError
     */
    public deleteTestVinteoApiV1Record({
        id,
    }: {
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/test/vinteo/api/v1/record/{id}',
            path: {
                'id': id,
            },
        });
    }

}
