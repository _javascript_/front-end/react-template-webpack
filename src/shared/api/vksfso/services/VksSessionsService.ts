/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ConferenceConnectDto } from '../models/ConferenceConnectDto';
import type { ParticipantConnectedRequest } from '../models/ParticipantConnectedRequest';
import type { ParticipantConnectedResponse } from '../models/ParticipantConnectedResponse';
import type { SessionInfoDto } from '../models/SessionInfoDto';
import type { SessionParticipantDto } from '../models/SessionParticipantDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class VksSessionsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Возвращает информацию о подключении к конференции.
     * @returns ConferenceConnectDto Success
     * @throws ApiError
     */
    public getApiVksSessionsConnectInfo({
        link,
    }: {
        /**
         * Ссылка на конференцию.
         */
        link: string,
    }): CancelablePromise<ConferenceConnectDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/vks-sessions/connect-info/{link}',
            path: {
                'link': link,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Добавляет запись о подключившемся участнике.
     * @returns ParticipantConnectedResponse Success
     * @throws ApiError
     */
    public postApiVksSessionsParticipantConnected({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.ParticipantConnectedRequest" />
         */
        requestBody?: ParticipantConnectedRequest,
    }): CancelablePromise<ParticipantConnectedResponse> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/participant-connected',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает информацию о сессии.
     * @returns SessionInfoDto Success
     * @throws ApiError
     */
    public getApiVksSessionsInfo({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<SessionInfoDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/vks-sessions/{sessionId}/info',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список участников сессии.
     * @returns SessionParticipantDto Success
     * @throws ApiError
     */
    public getApiVksSessionsParticipants({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<Array<SessionParticipantDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/vks-sessions/{sessionId}/participants',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Отключает микрофон участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsMuteSelf({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/mute-self',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Включает микрофон участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsUnmuteSelf({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/unmute-self',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Отключает камеру участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsDisableSelfCam({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/disable-self-cam',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Включает камеру участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsEnableSelfCam({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/enable-self-cam',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Отключает микрофон участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsMute({
        sessionId,
        participantId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
        /**
         * Идентификатор участника.
         */
        participantId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/mute/{participantId}',
            path: {
                'sessionId': sessionId,
                'participantId': participantId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Включает микрофон участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsUnmute({
        sessionId,
        participantId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
        /**
         * Идентификатор участника.
         */
        participantId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/unmute/{participantId}',
            path: {
                'sessionId': sessionId,
                'participantId': participantId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Отключает камеру участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsDisableCam({
        sessionId,
        participantId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
        /**
         * Идентификатор участника.
         */
        participantId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/disable-cam/{participantId}',
            path: {
                'sessionId': sessionId,
                'participantId': participantId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Включает камеру участника.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsEnableCam({
        sessionId,
        participantId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
        /**
         * Идентификатор участника.
         */
        participantId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/enable-cam/{participantId}',
            path: {
                'sessionId': sessionId,
                'participantId': participantId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Завершает сессию.
     * @returns any Success
     * @throws ApiError
     */
    public postApiVksSessionsFinish({
        sessionId,
    }: {
        /**
         * Идентификатор сессии.
         */
        sessionId: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/vks-sessions/{sessionId}/finish',
            path: {
                'sessionId': sessionId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

}
