/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateOrganizationDto } from '../models/CreateOrganizationDto';
import type { Int64IdTitleDto } from '../models/Int64IdTitleDto';
import type { OrganizationDto } from '../models/OrganizationDto';
import type { PagedOrganizationDtoPaged } from '../models/PagedOrganizationDtoPaged';
import type { RemoveOrganizationsDto } from '../models/RemoveOrganizationsDto';
import type { SimpleUserDto } from '../models/SimpleUserDto';
import type { SingleOrganizationDto } from '../models/SingleOrganizationDto';
import type { SubgroupUserCountDto } from '../models/SubgroupUserCountDto';
import type { UpdateOrganizationDto } from '../models/UpdateOrganizationDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class OrganizationsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Создает организацию.
     * @returns any Success
     * @throws ApiError
     */
    public postApiOrganizations({
        requestBody,
    }: {
        /**
         * Данные для создания организации.
         */
        requestBody?: CreateOrganizationDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/organizations',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список организаций.
     * @returns OrganizationDto Success
     * @throws ApiError
     */
    public getApiOrganizations(): CancelablePromise<Array<OrganizationDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/organizations',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Обновляет организацию.
     * @returns any Success
     * @throws ApiError
     */
    public putApiOrganizations({
        id,
        requestBody,
    }: {
        /**
         * Идентификатор организации.
         */
        id: number,
        /**
         * Данные для обновления организации.
         */
        requestBody?: UpdateOrganizationDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/organizations/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает организацию по идентификатору.
     * @returns SingleOrganizationDto Success
     * @throws ApiError
     */
    public getApiOrganizations1({
        id,
    }: {
        /**
         * Идентификатор.
         */
        id: number,
    }): CancelablePromise<SingleOrganizationDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/organizations/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет организацию.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiOrganizations({
        id,
    }: {
        /**
         * Идентификатор.
         */
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/organizations/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список организаций (упрощенный).
     * @returns Int64IdTitleDto Success
     * @throws ApiError
     */
    public getApiOrganizationsSimple(): CancelablePromise<Array<Int64IdTitleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/organizations/simple',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает пагинированный список организаций.
     * @returns PagedOrganizationDtoPaged Success
     * @throws ApiError
     */
    public getApiOrganizationsPaged({
        search,
        sortBy,
        page,
        perPage,
    }: {
        /**
         * Возвращает или задает комплексный поиск по имени или идентификатору.
         */
        search?: string,
        /**
         * Возвращает или задает сортировку.
         */
        sortBy?: Array<string>,
        /**
         * Возвращает или задает номер страницы.
         */
        page?: number,
        /**
         * Возвращает или задает количество элементов на странице.
         */
        perPage?: number,
    }): CancelablePromise<PagedOrganizationDtoPaged> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/organizations/paged',
            query: {
                'search': search,
                'sortBy': sortBy,
                'page': page,
                'perPage': perPage,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список подгрупп по идентификатору организации.
     * @returns SubgroupUserCountDto Success
     * @throws ApiError
     */
    public getApiOrganizationsSubgroups({
        organizationId,
    }: {
        /**
         * Идентификатор организации.
         */
        organizationId: number,
    }): CancelablePromise<Array<SubgroupUserCountDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/organizations/{organizationId}/subgroups',
            path: {
                'organizationId': organizationId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет несколько организаций.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiOrganizationsMultiple({
        requestBody,
    }: {
        /**
         * Данные для удаления.
         */
        requestBody?: RemoveOrganizationsDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/organizations/multiple',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список пользователей организации.
     * @returns SimpleUserDto Success
     * @throws ApiError
     */
    public getApiOrganizationsUsers({
        organizationId,
    }: {
        /**
         * Идентификатор организации.
         */
        organizationId: number,
    }): CancelablePromise<Array<SimpleUserDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/organizations/{organizationId}/users',
            path: {
                'organizationId': organizationId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
