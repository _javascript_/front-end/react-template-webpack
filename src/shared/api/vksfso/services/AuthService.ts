/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { SignedInDto } from '../models/SignedInDto';
import type { SignInDto } from '../models/SignInDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class AuthService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Производит авторизацию/аутентификацию пользователя.
     * @returns SignedInDto Success
     * @throws ApiError
     */
    public postApiAuthSignIn({
        requestBody,
    }: {
        /**
         * Данные пользователя для авторизации/аутентификации.
         */
        requestBody?: SignInDto,
    }): CancelablePromise<SignedInDto> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/auth/sign-in',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
