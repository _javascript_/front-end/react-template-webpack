/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreateMeetingDto } from '../models/CreateMeetingDto';
import type { DetailedMeetingDto } from '../models/DetailedMeetingDto';
import type { GetOverlapConferenceHallsRequest } from '../models/GetOverlapConferenceHallsRequest';
import type { OverlapConferenceHallDto } from '../models/OverlapConferenceHallDto';
import type { PagedMeetingDtoPaged } from '../models/PagedMeetingDtoPaged';
import type { PagedParticipantDtoPaged } from '../models/PagedParticipantDtoPaged';
import type { StringIdTitleDto } from '../models/StringIdTitleDto';
import type { UpdateMeetingDto } from '../models/UpdateMeetingDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class MeetingsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Создает экземпляр конференции.
     * @returns DetailedMeetingDto Success
     * @throws ApiError
     */
    public postApiMeetings({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.MeetingDtos.CreateMeetingDto" path="/summary/node()" />
         */
        requestBody?: CreateMeetingDto,
    }): CancelablePromise<DetailedMeetingDto> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/meetings',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Обновляет экземпляр конференции.
     * @returns any Success
     * @throws ApiError
     */
    public putApiMeetings({
        id,
        requestBody,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.MeetingDtos.UpdateMeetingDto" path="/summary/node()" />
         */
        requestBody?: UpdateMeetingDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/meetings/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает экземпляр конференции.
     * @returns DetailedMeetingDto Success
     * @throws ApiError
     */
    public getApiMeetings({
        id,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
    }): CancelablePromise<DetailedMeetingDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/meetings/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Присваивает конференции статус "Удаленная" и удаляет все связанные с ней календарные события
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiMeetingsRemove({
        id,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/meetings/{id}/remove',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Присваивает конференции и оставшимся запланированным календарным событиям статус "Отменена"
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiMeetingsCancel({
        id,
    }: {
        /**
         * <inheritdoc cref="P:VKS.FSO.Core.Entities.IEntity`1.Id" path="/summary/node()" />
         */
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/meetings/{id}/cancel',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает пагинированный список конференций.
     * @returns PagedMeetingDtoPaged Success
     * @throws ApiError
     */
    public getApiMeetingsPaged({
        search,
        organizationTitle,
        status,
        privacy,
        sortBy,
        page,
        perPage,
    }: {
        /**
         * Возвращает или задает комплексный поиск по имени или идентификатору.
         */
        search?: string,
        /**
         * Возвращает или задает идентификатор организации.
         */
        organizationTitle?: string,
        /**
         * Возвращает или задает статус.
         */
        status?: string,
        /**
         * Режим конфиденциальности
         */
        privacy?: string,
        /**
         * Возвращает или задает сортировку.
         */
        sortBy?: Array<string>,
        /**
         * Возвращает или задает номер страницы.
         */
        page?: number,
        /**
         * Возвращает или задает количество элементов на странице.
         */
        perPage?: number,
    }): CancelablePromise<PagedMeetingDtoPaged> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/meetings/paged',
            query: {
                'search': search,
                'organizationTitle': organizationTitle,
                'status': status,
                'privacy': privacy,
                'sortBy': sortBy,
                'page': page,
                'perPage': perPage,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список возможных статусов конференции.
     * @returns StringIdTitleDto Success
     * @throws ApiError
     */
    public getApiMeetingsConferenceStatuses(): CancelablePromise<Array<StringIdTitleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/meetings/conference-statuses',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список возможных статусов приватности конференции.
     * @returns StringIdTitleDto Success
     * @throws ApiError
     */
    public getApiMeetingsPrivacyTypes(): CancelablePromise<Array<StringIdTitleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/meetings/privacy-types',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список конференц-залов имеющих пересечения по расписанию.
     * @returns OverlapConferenceHallDto Success
     * @throws ApiError
     */
    public postApiMeetingsOverlapConferenceHalls({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.GetOverlapConferenceHallsRequest" />
         */
        requestBody?: GetOverlapConferenceHallsRequest,
    }): CancelablePromise<Array<OverlapConferenceHallDto>> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/meetings/overlap-conference-halls',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает пагинированный список участников для добавления в конференцию.
     * @returns PagedParticipantDtoPaged Success
     * @throws ApiError
     */
    public getApiMeetingsParticipants({
        organizationId,
        subgroups,
        users,
        organizationAdmin,
        sortBy,
        page,
        perPage,
    }: {
        /**
         * Идентификатор организации.
         */
        organizationId?: number,
        /**
         * Список подгрупп (наименование или идентификатор).
         */
        subgroups?: Array<string>,
        /**
         * Список пользователей (ФИО или идентификатор).
         */
        users?: Array<string>,
        /**
         * Флаг фильтрации только администраторов организации.
         */
        organizationAdmin?: boolean,
        /**
         * Возвращает или задает сортировку.
         */
        sortBy?: Array<string>,
        /**
         * Возвращает или задает номер страницы.
         */
        page?: number,
        /**
         * Возвращает или задает количество элементов на странице.
         */
        perPage?: number,
    }): CancelablePromise<PagedParticipantDtoPaged> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/meetings/participants',
            query: {
                'organizationId': organizationId,
                'subgroups': subgroups,
                'users': users,
                'organizationAdmin': organizationAdmin,
                'sortBy': sortBy,
                'page': page,
                'perPage': perPage,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
