/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BookConferenceHallDto } from '../models/BookConferenceHallDto';
import type { ConferenceHallScheduleDto } from '../models/ConferenceHallScheduleDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class ConferenceHallSchedulesService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Возвращает список элементов расписания конференц-залов за интервал.
     * @returns ConferenceHallScheduleDto Success
     * @throws ApiError
     */
    public getApiConferenceHallSchedules({
        start,
        end,
        organizationId,
        address,
        hallIds,
    }: {
        /**
         * Начало интервала.
         */
        start?: string,
        /**
         * Окончание интервала.
         */
        end?: string,
        /**
         * Идентификатор организации.
         */
        organizationId?: number,
        /**
         * Адрес конференц зала.
         */
        address?: string,
        /**
         * Список идентификаторов конференц-залов.
         */
        hallIds?: Array<number>,
    }): CancelablePromise<Array<ConferenceHallScheduleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/conference-hall-schedules',
            query: {
                'start': start,
                'end': end,
                'organizationId': organizationId,
                'address': address,
                'hallIds': hallIds,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Создает бронирование конференц-зала.
     * @returns ConferenceHallScheduleDto Success
     * @throws ApiError
     */
    public postApiConferenceHallSchedules({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Dtos.BookConferenceHallDto" path="/summary/node()" />
         */
        requestBody?: BookConferenceHallDto,
    }): CancelablePromise<ConferenceHallScheduleDto> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/conference-hall-schedules',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет бронирование конференц-зала.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiConferenceHallSchedules({
        id,
    }: {
        /**
         * Идентификатор бронирования.
         */
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/conference-hall-schedules/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
