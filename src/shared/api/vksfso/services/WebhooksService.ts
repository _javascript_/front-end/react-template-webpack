/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { WebhookEvent } from '../models/WebhookEvent';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class WebhooksService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Обрабатывает события от вебхука vinteo
     * @returns any Success
     * @throws ApiError
     */
    public postApiWebhooksVinteo({
        requestBody,
    }: {
        /**
         * <inheritdoc cref="T:VKS.FSO.Core.Vinteo.Models.Events.WebhookEvent" />
         */
        requestBody?: WebhookEvent,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/webhooks/vinteo',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
