/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AddUsersToSubgroupDto } from '../models/AddUsersToSubgroupDto';
import type { CreateSubgroupDto } from '../models/CreateSubgroupDto';
import type { Int64IdTitleDto } from '../models/Int64IdTitleDto';
import type { RemoveSubgroupsDto } from '../models/RemoveSubgroupsDto';
import type { RemoveUsersFromSubgroupDto } from '../models/RemoveUsersFromSubgroupDto';
import type { SimpleUserDto } from '../models/SimpleUserDto';
import type { SubgroupDto } from '../models/SubgroupDto';
import type { UpdateSubgroupDto } from '../models/UpdateSubgroupDto';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class SubgroupsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Создает подгруппу.
     * @returns SubgroupDto Success
     * @throws ApiError
     */
    public postApiSubgroups({
        requestBody,
    }: {
        /**
         * Данные для создания подгруппы.
         */
        requestBody?: CreateSubgroupDto,
    }): CancelablePromise<SubgroupDto> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/subgroups',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Обновляет подгруппу.
     * @returns any Success
     * @throws ApiError
     */
    public putApiSubgroups({
        id,
        requestBody,
    }: {
        /**
         * Идентификатор подгруппы.
         */
        id: number,
        /**
         * Данные для обновления подгруппы.
         */
        requestBody?: UpdateSubgroupDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/subgroups/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет подгруппу по идентификатору.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiSubgroups({
        id,
    }: {
        /**
         * Идентификатор.
         */
        id: number,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/subgroups/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает информацию о подгруппе.
     * @returns SubgroupDto Success
     * @throws ApiError
     */
    public getApiSubgroups({
        id,
    }: {
        /**
         * Идентификатор подгруппы.
         */
        id: number,
    }): CancelablePromise<SubgroupDto> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/subgroups/{id}',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет несколько подгрупп.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiSubgroupsMultiple({
        requestBody,
    }: {
        /**
         * Данные для удаления.
         */
        requestBody?: RemoveSubgroupsDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/subgroups/multiple',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Удаляет пользователей из подгруппы.
     * @returns any Success
     * @throws ApiError
     */
    public deleteApiSubgroupsRemoveUsers({
        requestBody,
    }: {
        /**
         * Данные для удаления.
         */
        requestBody?: RemoveUsersFromSubgroupDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/subgroups/remove-users',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список пользователей подгруппы.
     * @returns SimpleUserDto Success
     * @throws ApiError
     */
    public getApiSubgroupsUsers({
        id,
    }: {
        /**
         * Идентификатор подгруппы.
         */
        id: number,
    }): CancelablePromise<Array<SimpleUserDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/subgroups/{id}/users',
            path: {
                'id': id,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Добавляет пользователей в подгруппу.
     * @returns any Success
     * @throws ApiError
     */
    public postApiSubgroupsAddUsers({
        requestBody,
    }: {
        /**
         * Данные для добавления пользователей в подгруппу.
         */
        requestBody?: AddUsersToSubgroupDto,
    }): CancelablePromise<any> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/subgroups/add-users',
            body: requestBody,
            mediaType: 'application/json-patch+json',
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                404: `Not Found`,
                500: `Server Error`,
            },
        });
    }

    /**
     * Возвращает список упрощенных моделей подгрупп.
     * @returns Int64IdTitleDto Success
     * @throws ApiError
     */
    public getApiSubgroupsSimple({
        organizationId,
    }: {
        /**
         * Идентификатор организации.
         */
        organizationId?: number,
    }): CancelablePromise<Array<Int64IdTitleDto>> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/subgroups/simple',
            query: {
                'organizationId': organizationId,
            },
            errors: {
                400: `Bad Request`,
                401: `Unauthorized`,
                500: `Server Error`,
            },
        });
    }

}
