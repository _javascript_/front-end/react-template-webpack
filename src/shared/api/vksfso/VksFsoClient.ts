/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseHttpRequest } from './core/BaseHttpRequest';
import type { OpenAPIConfig } from './core/OpenAPI';
import { AxiosHttpRequest } from './core/AxiosHttpRequest';

import { AuthService } from './services/AuthService';
import { ConferenceHallsService } from './services/ConferenceHallsService';
import { ConferenceHallSchedulesService } from './services/ConferenceHallSchedulesService';
import { MeetingsService } from './services/MeetingsService';
import { OrganizationsService } from './services/OrganizationsService';
import { SchedulesService } from './services/SchedulesService';
import { SubgroupsService } from './services/SubgroupsService';
import { TestVinteoApiService } from './services/TestVinteoApiService';
import { UsersService } from './services/UsersService';
import { VksSessionsService } from './services/VksSessionsService';
import { WebhooksService } from './services/WebhooksService';

type HttpRequestConstructor = new (config: OpenAPIConfig) => BaseHttpRequest;

export class VksFsoClient {

    public readonly auth: AuthService;
    public readonly conferenceHalls: ConferenceHallsService;
    public readonly conferenceHallSchedules: ConferenceHallSchedulesService;
    public readonly meetings: MeetingsService;
    public readonly organizations: OrganizationsService;
    public readonly schedules: SchedulesService;
    public readonly subgroups: SubgroupsService;
    public readonly testVinteoApi: TestVinteoApiService;
    public readonly users: UsersService;
    public readonly vksSessions: VksSessionsService;
    public readonly webhooks: WebhooksService;

    public readonly request: BaseHttpRequest;

    constructor(config?: Partial<OpenAPIConfig>, HttpRequest: HttpRequestConstructor = AxiosHttpRequest) {
        this.request = new HttpRequest({
            BASE: config?.BASE ?? '',
            VERSION: config?.VERSION ?? '1.0',
            WITH_CREDENTIALS: config?.WITH_CREDENTIALS ?? false,
            CREDENTIALS: config?.CREDENTIALS ?? 'include',
            TOKEN: config?.TOKEN,
            USERNAME: config?.USERNAME,
            PASSWORD: config?.PASSWORD,
            HEADERS: config?.HEADERS,
            ENCODE_PATH: config?.ENCODE_PATH,
        });

        this.auth = new AuthService(this.request);
        this.conferenceHalls = new ConferenceHallsService(this.request);
        this.conferenceHallSchedules = new ConferenceHallSchedulesService(this.request);
        this.meetings = new MeetingsService(this.request);
        this.organizations = new OrganizationsService(this.request);
        this.schedules = new SchedulesService(this.request);
        this.subgroups = new SubgroupsService(this.request);
        this.testVinteoApi = new TestVinteoApiService(this.request);
        this.users = new UsersService(this.request);
        this.vksSessions = new VksSessionsService(this.request);
        this.webhooks = new WebhooksService(this.request);
    }
}

