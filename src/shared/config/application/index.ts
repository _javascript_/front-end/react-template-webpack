import { getEnvVar } from "../_base";

export const NODE_ENV = getEnvVar("NODE_ENV");
export const IS_DEV_ENV = NODE_ENV === "development";
export const IS_PROD_ENV = NODE_ENV === "production";
export const JSON_PLACEHOLDER_API_URI = getEnvVar("JSON_PLACEHOLDER_API_URI");
export const APP_TITLE = getEnvVar("APP_TITLE");
export const LOG_LEVEL = getEnvVar("LOG_LEVEL");
export const IS_HASH_ROUTER = getEnvVar("HASH_ROUTER", "bool");
