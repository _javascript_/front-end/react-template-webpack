import { ACCESS_LEVELS } from "../access";
import { LAYOUT_NAMES } from "../layouts";

import { PAGE_NAMES } from "./_names";
import { PageMetaData } from "./_types";

export const pageName = PAGE_NAMES.SIGN_IN;

export const path = () => `/auth/sign_in`;

export const metadata: PageMetaData = {
  pageName,
  layout: LAYOUT_NAMES.ONLY_CONTENT,
  access: {
    allowed: [ACCESS_LEVELS.GUEST],
    redirect: `/`,
  },
  path: path(),
};

export const redirects = {};
