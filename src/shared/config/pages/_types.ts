import { Level } from "../access";
import { LayoutName } from "../layouts";

import { PAGE_NAMES } from "./_names";

export type PageNames = typeof PAGE_NAMES;
export type PageName = PageNames[keyof PageNames];

export type PageMetaData = {
  pageName: PageName;
  path: string;
  layout: PageLayoutConfig;
  access?: Access;
};

export type PagesMetaData = {
  [name in PageName]?: PageMetaData;
};

type Redirect = {
  [key in Level]?: string;
};
type Access = {
  allowed?: Level[];
  redirect?: Redirect | string;
};
type PageLayoutConfig =
  | LayoutName
  | {
      [key in Level]?: LayoutName;
    };

export type PageConfig = {
  pageName: PageName;
  component: PageComponent;
};

export type PageComponent =
  | React.LazyExoticComponent<(props: any) => JSX.Element>
  | ((props: any) => JSX.Element);
