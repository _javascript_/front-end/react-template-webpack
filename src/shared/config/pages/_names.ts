export const PAGE_NAMES = {
  HOME: "home",
  ERRORS: "errors",
  SIGN_IN: "sign-in",
  SIGN_UP: "sign-up",
  EXAMPLE: "example",
} as const;
