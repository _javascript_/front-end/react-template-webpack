import { LAYOUT_NAMES } from "../layouts";

import { PAGE_NAMES } from "./_names";
import { PageMetaData } from "./_types";

export const pageName = PAGE_NAMES.EXAMPLE;

export const path = () => `/example`;

export const metadata: PageMetaData = {
  pageName,
  layout: LAYOUT_NAMES.HEADER_FOOTER,
  path: path(),
};

export const redirects = {};
