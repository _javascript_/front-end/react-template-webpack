import { LAYOUT_NAMES } from "../layouts";

import { PAGE_NAMES } from "./_names";
import { PageMetaData } from "./_types";

export const pageName = PAGE_NAMES.ERRORS;

export const path = (code = ":code") => `/errors/${code}` as const;

export const metadata: PageMetaData = {
  pageName,
  layout: LAYOUT_NAMES.ONLY_CONTENT,
  path: path(),
};

const redirectFrom = "/errors";
const redirectTo = path("404");

export const redirects = {
  [redirectFrom]: redirectTo,
};
