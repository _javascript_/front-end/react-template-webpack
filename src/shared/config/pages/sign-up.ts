import { ACCESS_LEVELS } from "../access";
import { LAYOUT_NAMES } from "../layouts";

import { PAGE_NAMES } from "./_names";
import { PageMetaData } from "./_types";
import { path as errorPath } from "./errors";

export const pageName = PAGE_NAMES.SIGN_UP;

export const path = () => `/auth/sign_up`;

export const metadata: PageMetaData = {
  pageName,
  layout: LAYOUT_NAMES.HEADER_FOOTER,
  access: {
    allowed: [ACCESS_LEVELS.GUEST],
    redirect: errorPath("403"),
  },
  path: path(),
};

export const redirects = {};
