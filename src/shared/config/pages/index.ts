import { PagesMetaData } from "./_types";

import * as errors from "./errors";
import * as example from "./example";
import * as home from "./home";

import * as signIn from "./sign-in";
import * as signUp from "./sign-up";


export * from "./_types";
export * from "./_names";

export const PAGES_META_DATA: PagesMetaData = {
  [home.pageName]: home.metadata,

  [errors.pageName]: errors.metadata,

  [signIn.pageName]: signIn.metadata,
  [signUp.pageName]: signUp.metadata,

  [example.pageName]: example.metadata,
};

export const REDIRECTS_PAGES = {
  ...home.redirects,
  ...errors.redirects,

  ...signIn.redirects,
  ...signUp.redirects,
};

export const paths = {
  [home.pageName]: home.path,
  [errors.pageName]: errors.path,

  [signIn.pageName]: signIn.path,
  [signUp.pageName]: signUp.path,

  [example.pageName]: example.path,
};

export type Paths = typeof paths;
export type Path = Paths[keyof Paths];
