import { ACCESS_LEVELS } from "../access";
import { LAYOUT_NAMES } from "../layouts";

import { PAGE_NAMES } from "./_names";
import { PageMetaData } from "./_types";

import { path as signInPath } from "./sign-in";

export const pageName = PAGE_NAMES.HOME;

export const path = () => `/`;

export const metadata: PageMetaData = {
  pageName,
  layout: LAYOUT_NAMES.HEADER_FOOTER,
  access: {
    allowed: [ACCESS_LEVELS.GUEST, ACCESS_LEVELS.USER, ACCESS_LEVELS.ADMIN],
    redirect: signInPath(),
  },
  path: path(),
};

export const redirects = {};
