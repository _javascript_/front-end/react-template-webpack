export const ACCESS_LEVELS = {
  GUEST: 1,
  USER: 2,
  ADMIN: 3,
  DEVELOPER: 4,
} as const;
export type AccessLevels = typeof ACCESS_LEVELS;
export type AccessKyes = keyof AccessLevels;
export type Level = AccessLevels[keyof AccessLevels];
