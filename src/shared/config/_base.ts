export function getEnvVar(key: string, type: "bool"): boolean;
export function getEnvVar(key: string, type: "string"): string;
export function getEnvVar(key: string): string;
export function getEnvVar(key: string, type: "bool" | "string" = "string") {
  if (process.env[key] === undefined) {
    throw new Error(`Env variable ${key} is required`);
  }
  const data = process.env[key] || "";
  if (type === "string") return data;
  if (data === "true") return true;
  return false;
}
