export const LAYOUT_NAMES = {
  HEADER: "header",
  HEADER_FOOTER: "header-footer",
  HEADER_LEFTBAR: "header-leftbar",
  HEADER_LEFTBAR_FOOTER: "header-leftbar-footer",
  ONLY_CONTENT: "only-content",
} as const;
export type LayoutNames = typeof LAYOUT_NAMES;
export type LayoutName = LayoutNames[keyof LayoutNames];
