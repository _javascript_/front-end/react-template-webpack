import { useNavigate } from "react-router";

import { PAGE_NAMES, paths } from "shared/config/pages";

export function useAppNavigate() {
  const navigate = useNavigate();

  const goToBack = () => navigate(-1);

  const goToHomePage = () => navigate(paths[PAGE_NAMES.HOME]());

  const goToErrorPage = (code: string) =>
    navigate(paths[PAGE_NAMES.ERRORS](code));

  return {
    goToBack,
    goToHomePage,
    goToErrorPage,
  };
}
