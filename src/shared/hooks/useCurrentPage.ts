import { useMemo } from "react";
import { useLocation, useParams } from "react-router";

import { getPageNameByPath } from "shared/utils/getPageNameByPath";

export function useCurrentPage() {
  const location = useLocation();
  const params = useParams();
  const pageName = useMemo(() => {
    return getPageNameByPath(location.pathname, params);
  }, [location, params]);

  return pageName;
}
