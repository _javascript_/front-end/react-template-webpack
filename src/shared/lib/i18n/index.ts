import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import { ru, en } from "./resources";

const resources = { ru, en, dev: {} };

i18n.use(initReactI18next).init({
  resources,
  lng: "ru",
  // fallbackLng: "ru",
  interpolation: {
    escapeValue: false,
  },
});

export * from "./hooks";
export default i18n;
