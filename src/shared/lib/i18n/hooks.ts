import { useTranslation } from "react-i18next";

export const useHelmetTranslation = (pageName: string) => {
  const { t } = useTranslation("helmet");
  return {
    title: t(`${pageName}.title`),
    description: t(`${pageName}.description`),
  };
};
