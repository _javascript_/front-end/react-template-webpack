import { counts, todos, viewer } from "./entities";
import { errors } from "./errors";
import { helmet } from "./helmet";
import { pages } from "./pages";

export const en = {
  pages,
  helmet,
  errors,
  counts,
  todos,
  viewer,
};
