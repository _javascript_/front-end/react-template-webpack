import { APP_TITLE } from "shared/config/application";
import { defaultHelmet } from "../../utils";

const helmetWithPrefix = (name: string) => {
  return defaultHelmet(name, APP_TITLE);
};

export const helmet = {
  home: helmetWithPrefix("home"),
  protected: helmetWithPrefix("protected"),
  unprotected: helmetWithPrefix("unprotected"),
  sign_in: helmetWithPrefix("sign_in"),
  sign_up: helmetWithPrefix("  ign_up"),
  errors: helmetWithPrefix("errors"),
};
