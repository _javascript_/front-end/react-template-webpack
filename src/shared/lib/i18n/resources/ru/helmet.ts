import { APP_TITLE } from "shared/config/application";
import { defaultHelmet } from "../../utils";

const helmetWithPrefix = (name: string) => {
  return defaultHelmet(name, APP_TITLE);
};

export const helmet = {
  home: helmetWithPrefix("Главная"),
  protected: helmetWithPrefix("Защищённая"),
  development: helmetWithPrefix("Разработка"),
  todos: helmetWithPrefix("Список задач"),
  todos_details: helmetWithPrefix("Подробности задачи"),
  unprotected: helmetWithPrefix("Не защищённая"),
  sign_in: helmetWithPrefix("Авторизация"),
  sign_up: helmetWithPrefix("Регистрация"),
  errors: helmetWithPrefix("Ошибка"),
};
