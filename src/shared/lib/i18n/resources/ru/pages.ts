export const pages = {
  home: "Главная",
  development: "Разработка",
  todos: "Список задач",
  todo_details: "Подробности задачи",
  errors: "Ошибка",
};
