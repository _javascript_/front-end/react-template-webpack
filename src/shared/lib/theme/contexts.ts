import React from "react";

export const ColorModeContext = React.createContext({
  toggleColorMode: () => {},
});
export const ColorOptionsContext = React.createContext({ setColors: () => {} });
