import React from "react";
import { ColorModeContext } from "./contexts";

export const useColorMode = () => React.useContext(ColorModeContext);
