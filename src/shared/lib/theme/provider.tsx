
import { ThemeProvider as MuiThemeProvider } from "@emotion/react";
import { createTheme, PaletteMode } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import React, { ReactNode } from "react";

import { ColorModeContext } from "./contexts";
import { getDesignSystem } from "./design-system";

export type ThemeProviderProps = {
  children: ReactNode;
};

export function ThemeProvider({ children }: ThemeProviderProps) {
  const [mode, setMode] = React.useState<PaletteMode>("light");
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode: PaletteMode) =>
          prevMode === "light" ? "dark" : "light",
        );
      },
    }),
    [],
  );

  const theme = React.useMemo(
    () => createTheme(getDesignSystem({ mode })),
    [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <CssBaseline />
      <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
    </ColorModeContext.Provider>
  );
}
