import { PaletteMode } from "@mui/material";

// export type DesignSystem = {
//   mode: PaletteMode;
// };

export type Options = {
  mode: PaletteMode;
};
export const getDesignSystem = (options: Options) => {
  const { mode } = options;

  const palette = mode === "light" ? {} : {};

  return { palette: { mode, ...palette } };
};
