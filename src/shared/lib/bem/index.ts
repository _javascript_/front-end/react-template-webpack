type BemConfig = { n: string; e: string; m: string };
const CONFIG: BemConfig = { n: "", e: "__", m: "_" };

type Modifiers = {
  [m: string]: string | boolean | undefined;
};
function objectModifiersToString(
  modifiers: Modifiers,
  separator: string,
): string {
  const result = Object.entries(modifiers)
    .filter(([, value]) => Boolean(value))
    .map(([key, value]) => {
      if (typeof value === "boolean") return key;
      return `${key}${separator}${value}`;
    })
    .join(separator);
  return result;
}

type BlockElementModifier = {
  block: string;
  element?: string;
  modifiers?: Modifiers;
};
// eslint-disable-next-line @typescript-eslint/no-shadow
function format(bem: BlockElementModifier): string {
  const config = { ...CONFIG, ...this };
  const block = `${config.n}${bem.block}`;
  const element = bem.element ? `${config.e}${bem.element}` : "";
  const mStr = bem.modifiers
    ? objectModifiersToString(bem.modifiers, config.m)
    : "";
  if (bem.modifiers && !mStr) {
    return "";
  }
  const modifier = bem.modifiers ? `${config.m}${mStr}` : "";
  return `${block}${element}${modifier}`;
}

type Collect = {
  (): string;
  (element: string): string;
  (modifiers: Modifiers): string;
  (element: string, modifiers?: Modifiers): string;
};

type Bem = (block: string) => Collect;
export function bem(block: string): Collect {
  // @ts-ignore
  return (el1, el2) => {
    if (!el1 && !el2) return format.call(this, { block });
    if (typeof el1 === "string") {
      if (el2) {
        return format.call(this, { block, element: el1, modifiers: el2 });
      }
      return format.call(this, { block, element: el1 });
    }
    return format.call(this, { block, modifiers: el1 });
  };
}

export function withNaming(config: Partial<BemConfig>): Bem {
  return (block: string) => bem.call(config, block);
}
