import React from "react";
import { Provider } from "react-redux";

type StoreProviderProps = {
  children: React.ReactNode;
  store: any;
};

export function StoreProvider(props: StoreProviderProps) {
  const { children, store } = props;
  return <Provider store={store}>{children}</Provider>;
}
