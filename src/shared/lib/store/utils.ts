import {
  AsyncThunk,
  AsyncThunkOptions,
  AsyncThunkPayloadCreator,
  createAsyncThunk as rtkCreateAsyncThunk,
} from "@reduxjs/toolkit";

export function createAsyncThunk<Returned, ThunkArg = void>(
  typePrefix: string,
  payloadCreator: AsyncThunkPayloadCreator<
    Returned,
    ThunkArg,
    { state: RootState }
  >,
  options?: AsyncThunkOptions<ThunkArg, { state: RootState }>,
): AsyncThunk<Returned, ThunkArg, { state: RootState }> {
  return rtkCreateAsyncThunk(typePrefix, payloadCreator, options);
}
