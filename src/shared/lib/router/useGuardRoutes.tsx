import React, { useMemo } from "react";
import { RouteObject, Navigate, useLocation } from "react-router";
import { REDIRECTS_PAGES, PAGE_NAMES, paths } from "shared/config/pages";
import { Level } from "shared/config/access";

import { RouteConfig } from "./types";

const FORBIDDEN = paths[PAGE_NAMES.ERRORS]("403");
const UNAUTHORIZED = paths[PAGE_NAMES.ERRORS]("401");

function RedirectElement({ redirect, path }: any) {
  return <Navigate to={redirect} replace state={{ prevPath: path }} />;
}

function haveAccess(currentLevels: Level[], turegetLevels: Level[]): boolean {
  return currentLevels.some((lvl) =>
    turegetLevels.some((targetLvl) => lvl === targetLvl),
  );
}

export const useGuardRoutes = (
  configs: RouteConfig[],
  currentAccessLevels: Level[] = [],
): RouteObject[] => {
  const location = useLocation();

  const prevPath: string | null = useMemo(() => {
    return (location.state as any)?.prevPath ?? null;
  }, [location.state]);

  const pages = configs.map(({ metadata, route }) => {
    const { access } = metadata;
    if (!access || !access.allowed || !access.allowed.length) return route;

    let { element } = route;

    if (!haveAccess(currentAccessLevels, access.allowed)) {
      let redirect: string = UNAUTHORIZED;
      if (access.redirect) {
        if (typeof access.redirect === "string") {
          redirect = access.redirect;
        } else {
          const maxLevel = Math.max(...currentAccessLevels) as Level;
          redirect = access.redirect[maxLevel] ?? FORBIDDEN;
        }
      }
      element = (
        <RedirectElement
          redirect={prevPath && prevPath !== route.path ? prevPath : redirect}
          path={route.path}
        />
      );
    }

    return { ...route, element };
  });

  return pages;
};

export const redirectsRouteObject: RouteObject[] = Object.entries(
  REDIRECTS_PAGES,
).map(([from, to]) => {
  const element = <RedirectElement redirect={to} path={from} />;
  const route: RouteObject = { path: from, element };
  return route;
});
