import { RouteObject } from "react-router";

import { PageConfig, PAGES_META_DATA } from "shared/config/pages";

import { RouteConfig } from "./types";

import { withSuspenseAndMetadata } from "./withSuspenseAndMetadata";

function getRouteObject(config: PageConfig): RouteObject {
  const { component, pageName } = config;
  const metadata = PAGES_META_DATA[pageName];
  const element = withSuspenseAndMetadata(component, metadata);
  return { path: metadata.path, element, index: metadata.path === "/" };
}

export function getRouteConfigs(configs: PageConfig[]): RouteConfig[] {
  return configs.map((config) => {
    const { pageName } = config;
    const route = getRouteObject(config);
    return { route, metadata: PAGES_META_DATA[pageName] };
  });
}
