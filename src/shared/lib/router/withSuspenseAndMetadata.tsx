import React from "react";
import { PageMetaData } from "shared/config/pages";
import { Loader } from "shared/ui";
import { InjectionProps } from "./types";

export function withSuspenseAndMetadata(
  Component: React.FC<InjectionProps>,
  metadata: PageMetaData,
) {
  return (
    <React.Suspense fallback={<Loader />}>
      <Component metadata={metadata} />
    </React.Suspense>
  );
}
