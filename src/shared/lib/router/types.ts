import { RouteObject } from "react-router";
import { PageMetaData } from "shared/config/pages";

export type InjectionProps = {
  metadata: PageMetaData;
};

export type RouteConfig = {
  route: RouteObject;
  metadata: PageMetaData;
};
