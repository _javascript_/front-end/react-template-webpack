import { Item } from "../types";

export function checkItem(item: Item): boolean {
  if (!item) return false;

  if (typeof item === "string") return !!item.trim();

  if (typeof item === "object") {
    if (Array.isArray(item)) return !!item.filter(checkItem).length;
    const items = Object.values(item);
    if (!items.length) return false;
    return !!items.filter(checkItem).length;
  }

  return true;
}
