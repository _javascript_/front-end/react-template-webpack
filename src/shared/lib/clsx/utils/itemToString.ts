import { Item } from "../types";

export function itemToString(item: Item): string {
  if (!item) return "";

  if (typeof item === "string") return item.trim();
  if (typeof item === "object") {
    if (Array.isArray(item))
      return item.map(itemToString).filter(Boolean).join(" ");
    const items = Object.entries(item);
    if (!items.length) return "";
    return items
      .filter(([, v]) => !!v)
      .map(([c]) => c.trim())
      .filter(Boolean)
      .join(" ");
  }
  return "";
}
