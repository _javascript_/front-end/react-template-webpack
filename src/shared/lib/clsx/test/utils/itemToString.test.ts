import { itemToString } from "../../utils";

describe("itemToStrings", () => {
  test("check: string", () => {
    expect(itemToString("test")).toBe("test");
  });
  test("check: string", () => {
    expect(itemToString("   ")).toBe("");
  });

  test("check: string", () => {
    expect(itemToString(["test", "test"])).toBe("test test");
  });

  test("check: string", () => {
    expect(itemToString({ test: false })).toBe("");
  });

  test("check: string", () => {
    expect(itemToString({ test: true })).toBe("test");
  });

  test("check: string", () => {
    expect(itemToString({ test: true, test2: true, test3: false })).toBe(
      "test test2",
    );
  });

  test("check: string", () => {
    expect(itemToString(["test", { test: false }])).toBe("test");
  });
});
