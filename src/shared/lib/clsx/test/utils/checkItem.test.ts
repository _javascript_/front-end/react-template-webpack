import { checkItem } from "../../utils";

describe("checkItems", () => {
  test("check: string", () => {
    expect(checkItem("test")).toBe(true);
  });

  test("check: object", () => {
    expect(checkItem({ test: true })).toBe(true);
  });

  test("check: object", () => {
    expect(checkItem({ test: false })).toBe(false);
  });

  test("check: array", () => {
    expect(checkItem(["test"])).toBe(true);
  });

  test("check: array", () => {
    expect(checkItem([""])).toBe(false);
  });

  test("check: array", () => {
    expect(checkItem([{ test: false }])).toBe(false);
  });

  test("check: array", () => {
    expect(checkItem(["5", { test: false }])).toBe(true);
  });
});
