import { clsx } from "../main";

describe("clsx", () => {
  test("check: string", () => {
    expect(clsx("test")).toBe("test");
  });
  test("check: object", () => {
    expect(clsx("test", { test: true })).toBe("test test");
  });
  test("check: object", () => {
    expect(clsx("test", { test: false })).toBe("test");
  });
});
