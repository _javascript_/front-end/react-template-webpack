import { Items } from "./types";
import { checkItem, itemToString } from "./utils";


export function clsx(...items: Items): string {
  return items.filter(checkItem).map(itemToString).filter(Boolean).join(" ");
}
