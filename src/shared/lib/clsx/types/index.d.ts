type StringItem = string;
type ObjectItem = {
  [className: string]: any;
};
export type Item = StringItem | ObjectItem;
export type Items = Item[];
