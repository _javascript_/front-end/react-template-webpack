import { PageName, PAGE_NAMES, paths } from "shared/config/pages";

const mapPathToPageName = Object.fromEntries(
  Object.entries(paths).map(([name, fn]) => [fn(), name as PageName]),
);

export const getPageNameByPath = (
  pathname: string,
  params: { [kye: string]: string },
): PageName => {
  let path = pathname;
  if (Object.entries(params).length) {
    const [key, value] = Object.entries(params)[0];
    path = pathname.replace(value, `:${key}`);
  }
  return mapPathToPageName[path] ?? PAGE_NAMES.ERRORS;
};
