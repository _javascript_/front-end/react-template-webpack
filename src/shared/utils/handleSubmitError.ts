import { ApiError } from "shared/api/vksfso";

export function handleSubmitError(error: any) {
  if (error instanceof ApiError) {
    return error?.body?.detail ?? error.statusText;
  }
  return "Hz";
}
