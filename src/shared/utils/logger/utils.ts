export const Style = {
  test: [
    "color: red",
    "font-weight: 600",
    "padding: 2px 4px",
    "border-radius: 2px",
  ],
  base: [
    "color: #eee",
    "font-weight: 600",
    "padding: 2px 4px",
    "border-radius: 2px",
  ],
  error: ["background-color: coral"],
  warn: ["color: #444", "background-color: khaki"],
  info: ["background-color: deepskyblue"],
  debug: ["background-color: mediumseagreen"],
  log: ["color: #444", "background-color: lightgray"],
};

export const dateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
  hour12: false,
});
