import { LOG_LEVEL } from "shared/config/application";
import { dateTimeFormat, Style } from "./utils";

type TypeLVL = "Error" | "Warn" | "Info" | "Debug" | "Trace";

enum LVL {
  Error,
  Warn,
  Info,
  Debug,
  Trace,
}

export class Logger {
  private lvl: TypeLVL = "Trace";

  private scope: string;

  constructor(lvl: TypeLVL, scope: string = "") {
    this.lvl = lvl;
    this.scope = scope;
  }

  private nowDays() {
    const now = new Date();
    return dateTimeFormat.format(now);
  }

  private print(text: string, extra: string[], data?: object, lvl?: string) {
    let style = `${Style.base.join(";")};`;

    style += extra.join(";");

    let msg = `[${this.nowDays()}]`;
    if (lvl) msg += ` | ${lvl.toUpperCase()} |`;
    if (this.scope) msg += ` ${this.scope} ->`;
    if (text) msg += ` ${text}`;

    if (data) console.log(`%c${msg}`, style, data);
    else console.log(`%c${msg}`, style);
  }

  public error(text: string, data?: object) {
    this.print(text, Style.error, data, "Error");
  }

  public warn(text: string, data?: object) {
    if (LVL[this.lvl] >= 1) {
      this.print(text, Style.warn, data, "Warn");
    }
  }

  public info(text: string, data?: object) {
    if (LVL[this.lvl] >= 2) {
      this.print(text, Style.info, data, "Info");
    }
  }

  public debug(text: string, data?: object) {
    if (LVL[this.lvl] >= 3) {
      this.print(text, Style.debug, data, "Debug");
    }
  }

  public log(text: string, data?: object) {
    if (LVL[this.lvl] >= 4) {
      this.print(text, Style.log, data, " Log ");
    }
  }

  public static createScope(scope: string) {
    return new Logger(LOG_LEVEL as TypeLVL, scope);
  }

  public static create() {
    return new Logger(LOG_LEVEL as TypeLVL);
  }
}

export const logger = Logger.create();
