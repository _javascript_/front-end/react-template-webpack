function ucFirst(str: string) {
  return str[0].toUpperCase() + str.slice(1);
}

function ulFirst(str: string) {
  return str[0].toLowerCase() + str.slice(1);
}

function splitWords(string: string) {
  return string
    .replace(/\W+/g, " ")
    .split(/ |\B(?=[A-Z])/)
    .filter((word) => Boolean(word));
}

export function camelize(string: string) {
  return splitWords(string)
    .map((word, index) => (index ? ucFirst(word) : ulFirst(word)))
    .join("");
}

export function upperCase(string: string) {
  return splitWords(string)
    .map((word) => ucFirst(word))
    .join("");
}

export const snakeCase = (string: string) => {
  return splitWords(string)
    .map((word) => word.toLowerCase())
    .join("_");
};

export const skewerCase = (string: string) => {
  return splitWords(string)
    .map((word) => word.toLowerCase())
    .join("-");
};

export const constansCase = (string: string) => {
  return splitWords(string)
    .map((word) => word.toUpperCase())
    .join("_");
};
