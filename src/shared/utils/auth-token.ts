export class AuthToken {
  private static key = "access-token";

  static save(token: string) {
    localStorage.setItem(this.key, token);
  }

  static get() {
    return localStorage.getItem(this.key) ?? "";
  }

  static remove() {
    localStorage.removeItem(this.key);
  }
}
