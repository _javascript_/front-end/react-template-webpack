import { paths, PageName } from "shared/config/pages";
import { BreadcrumbItem } from "../types";

export const createItem = (page: PageName): BreadcrumbItem => {
  return { label: page, to: paths[page]() };
};
