import { PAGE_NAMES } from "shared/config/pages";

import { createItem } from "../utils";

const exampleItem = createItem(PAGE_NAMES.EXAMPLE);

export const example = [exampleItem];
