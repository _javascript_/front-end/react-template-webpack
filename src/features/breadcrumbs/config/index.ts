import { PAGE_NAMES } from "shared/config/pages";
import { Breadcrumbs, BreadcrumbsList } from "../types";

import { example } from "./example";

export const BREADCRUMBS: Breadcrumbs = {
  [PAGE_NAMES.EXAMPLE]: example,
};

export const SLICE_NAME = "breadcrumbs";
export const INITIAL_STATE: BreadcrumbsList = [{ label: "home", to: "/" }];
