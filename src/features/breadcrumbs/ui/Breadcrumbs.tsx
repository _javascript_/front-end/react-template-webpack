import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { BreadcrumbsList } from "../types";

import "./Breadcrumbs.css";

export type BreadcrumbsProps = {
  breadcrumbs: BreadcrumbsList;
};
export function Breadcrumbs({ breadcrumbs }: BreadcrumbsProps) {
  const { t } = useTranslation("pages");

  return (
    <div className="breadcrumbs">
      {breadcrumbs.map((breadcrumb, index) => {
        const { label, to } = breadcrumb;
        const isLast = breadcrumbs.length - 1 === index;
        if (isLast) {
          return (
            <h3 key={label} className="breadcrumbs__item">
              {t(label)}
            </h3>
          );
        }
        return (
          <Link
            key={label}
            to={to}
            className="breadcrumbs__item breadcrumbs__item_link"
          >
            {t(label)}
          </Link>
        );
      })}
    </div>
  );
}
