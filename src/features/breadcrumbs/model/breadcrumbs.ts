import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

import { INITIAL_STATE, SLICE_NAME } from "../config";
import { BreadcrumbsList } from "../types";

const slice = createSlice({
  name: SLICE_NAME,
  initialState: INITIAL_STATE,
  reducers: {
    set: (_, { payload }: PayloadAction<BreadcrumbsList>) => payload,
    reset: () => INITIAL_STATE,
  },
});

const useBreadcrumbs = () => {
  return useSelector(
    createSelector(
      (state: RootState) => state.viewer.access,
      (levels) => levels,
    ),
  );
};

export const actions = { ...slice.actions };
export const selectors = { useBreadcrumbs };
export const { reducer } = slice;
