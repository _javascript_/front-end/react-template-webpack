import { PageName } from "shared/config/pages";

export type BreadcrumbItem = {
  label: string;
  to: string;
};

export type Breadcrumbs = {
  [pageName in PageName]?: BreadcrumbItem[];
};
export type BreadcrumbsList = BreadcrumbItem[];
