import React from "react";
import { useTranslation } from "react-i18next";
import { useMatch } from "react-router";

import { PAGE_NAMES, paths } from "shared/config/pages";

import { Link } from "shared/ui";

type NavItemProps = {
  to: string;
  label: string;
  // items?: NavItemProps[];
};
function NavItem(props: NavItemProps) {
  const { label, to } = props;
  const isSelected = useMatch({ path: to, end: to === "/" });
  const { t } = useTranslation("pages");
  return (
    <Link selected={!!isSelected} to={to}>
      {t(label)}
    </Link>
  );
}

export const NavItemMemo = React.memo(NavItem);

const NAV_CONFIG: NavItemProps[] = [
  { label: PAGE_NAMES.HOME, to: paths[PAGE_NAMES.HOME]() },
  { label: PAGE_NAMES.EXAMPLE, to: paths[PAGE_NAMES.EXAMPLE]() },
];

function NavComponent() {
  return (
    <nav>
      {NAV_CONFIG.map((item) => (
        <NavItemMemo key={item.label} {...item} />
      ))}
    </nav>
  );
}

export const Nav = React.memo(NavComponent);
