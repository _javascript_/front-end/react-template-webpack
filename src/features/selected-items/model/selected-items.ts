import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

import {
  SELECTED_ITEMS_INITIAL_STATE,
  SELECTED_ITEMS_SLICE_NAME,
} from "../config";

type Entity = {
  id: number | string;
};

type SelectedItem<T extends Entity> = {
  entityName: string;
  entity: T;
};

const slice = createSlice({
  name: SELECTED_ITEMS_SLICE_NAME,
  initialState: SELECTED_ITEMS_INITIAL_STATE,
  reducers: {
    onToggleSelectedItem: (
      state,
      action: PayloadAction<SelectedItem<any>>,
    ) => {},
    selectItem: (state, { payload }: PayloadAction<SelectedItem<any>>) => {
      if (!state.entities[payload.entityName]) {
        state.entities[payload.entityName] = {};
      }
      if (!state.ids[payload.entityName]) state.ids[payload.entityName] = [];

      state.entities[payload.entityName][payload.entity.id] = payload.entity;
      state.ids[payload.entityName].push(payload.entity.id);
    },
    unSelectItem: (
      state,
      { payload }: PayloadAction<{ entityName: string; id: string | number }>,
    ) => {
      if (!state.entities[payload.entityName]) return;
      if (!state.ids[payload.entityName]) return;

      delete state.entities[payload.entityName][payload.id];
      state.ids[payload.entityName] = state.ids[payload.entityName].filter(
        (id) => id !== payload.id,
      );
    },
    reset: () => {
      return SELECTED_ITEMS_INITIAL_STATE;
    },
  },
});

type ReturnTypeEntities<T extends Entity> = {
  [id: string | number]: T;
};

const useEntities = <T extends Entity>(
  entityName: string,
): ReturnTypeEntities<T> => {
  return useSelector(
    createSelector(
      (state: RootState) => state.selectedItems.entities?.[entityName] ?? {},
      (entities) => entities as ReturnTypeEntities<T>,
    ),
  );
};

const useEntityIds = (entityName: string) => {
  return useSelector(
    createSelector(
      (state: RootState) => state.selectedItems.ids?.[entityName] ?? [],
      (ids) => ids,
    ),
  );
};

const useEntityById = <T extends Entity>(
  entityName: string,
  id: number | string,
): T | null => {
  return useSelector(
    createSelector(
      (state: RootState) => {
        const entities = state.selectedItems.entities?.[entityName] ?? {};
        return entities[id] ?? null;
      },
      (entity) => entity as T | null,
    ),
  );
};

export const actions = {
  ...slice.actions,
};
export const selectors = {
  useEntities,
  useEntityIds,
  useEntityById,
};
export const { reducer } = slice;
