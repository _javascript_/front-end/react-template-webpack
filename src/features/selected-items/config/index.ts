export const SELECTED_ITEMS_SLICE_NAME = "selected-items";
export const SELECTED_ITEMS_INITIAL_STATE: {
  entities: { [name: string]: { [id: string | number]: unknown } };
  ids: { [name: string]: (number | string)[] };
} = {
  entities: {},
  ids: {},
};
