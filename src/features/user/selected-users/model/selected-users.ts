import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

import { INITIAL_STATE, SELECTED_USERS } from "../config";

export const slice = createSlice({
  name: SELECTED_USERS,
  initialState: INITIAL_STATE,
  reducers: {
    setIds: (state, { payload }: PayloadAction<number[]>) => {
      state.ids = payload;
    },
    addId: (state, { payload }: PayloadAction<number>) => {
      state.ids.push(payload);
    },
    removeId: (state, { payload }: PayloadAction<number>) => {
      state.ids = state.ids.filter((item) => item !== payload);
    },
  },
});

const isSelectedItemsIsEmpty = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.selectedUsers.ids,
      (ids) => !ids.length,
    ),
  );

const useIds = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.selectedUsers.ids,
      (ids) => ids,
    ),
  );

export const actions = {
  ...slice.actions,
};
export const stores = {
  initialState: slice.getInitialState(),
};
export const selectors = {
  isSelectedItemsIsEmpty,
  useIds,
};

export const { reducer } = slice;
