import React, { useEffect } from "react";

namespace SelectedUsers {
  export type Props = {
    users: import("entities/user").userTypes.list.User[];
  };
}

function SelectedUsersComponent(props: SelectedUsers.Props) {
  const { users } = props;

  useEffect(() => {
    console.log(JSON.stringify(users));
  }, []);

  return <div>SelectedUsers</div>;
}

const SelectedUsers = React.memo(SelectedUsersComponent);

export { SelectedUsers };
