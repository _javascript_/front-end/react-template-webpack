import { bem as libBem } from "shared/lib";
import { SELECTED_USERS } from "../config";

export const bem = libBem(SELECTED_USERS);
