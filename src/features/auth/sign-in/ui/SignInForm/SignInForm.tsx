import { Button, CircularProgress, TextField, Typography } from "@mui/material";
import React from "react";
import { Control, Controller } from "react-hook-form";

import { clsx } from "shared/lib";

import { signInFormModel } from "../../model";
import { signInBem } from "../../utils";

type FormFieldProps = {
  name: string;
  control: Control;
  type?: string;
};

function FormFieldComponent(props: FormFieldProps) {
  const { control, name, type } = props;

  return (
    <Controller
      name={name}
      control={control}
      render={({ field, fieldState }) => {
        return (
          <div>
            <TextField
              {...field}
              type={type}
              error={!!fieldState.error}
              helperText={fieldState?.error?.message ?? ""}
            />
          </div>
        );
      }}
    />
  );
}

const FormField = React.memo(FormFieldComponent);

type SubmitButtonProps = {
  isSubmitting: boolean;
  isValid: boolean;
  submitError: string | null;
};
function SubmitButton(props: SubmitButtonProps) {
  const { submitError, isValid, isSubmitting } = props;

  return (
    <>
      <Button
        variant="outlined"
        type="submit"
        disabled={!isValid || isSubmitting}
      >
        Sign in {isSubmitting && <CircularProgress size="small" />}
      </Button>
      {submitError && (
        <Typography variant="caption" color="GrayText">
          {submitError}
        </Typography>
      )}
    </>
  );
}

function SignInTitleComponent() {
  return <Typography variant="h2">Sign in</Typography>;
}

const SignInTitle = React.memo(SignInTitleComponent);

namespace SignInForm {
  export type Props = {
    className?: string;
  };
}

function SignInForm(props: SignInForm.Props) {
  const { className } = props;

  const { control, isSubmitting, isValid, submitError, onSubmit } =
    signInFormModel.useSignInForm();

  const classes = clsx(signInBem("form"), className);

  return (
    <form onSubmit={onSubmit} className={classes}>
      <SignInTitle />
      <FormField name="email" control={control} />
      <FormField name="password" control={control} type="password" />

      <SubmitButton
        isSubmitting={isSubmitting}
        isValid={isValid}
        submitError={submitError}
      />
    </form>
  );
}

export { SignInForm };
