import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";

import { useForm } from "react-hook-form";

import { viewerModel } from "entities/viewer";
import { vksfso } from "shared/api";
import { useAppDispatch } from "shared/lib/store";

import { AuthToken } from "shared/utils/auth-token";
import { handleSubmitError } from "shared/utils/handleSubmitError";


import { schema } from "../config";
import { SignInDto } from "../types";

export function useSignInForm() {
  const dispatch = useAppDispatch();

  const [submitError, setSubmitError] = useState<string | null>(null);

  const methods = useForm<SignInDto>({
    resolver: yupResolver(schema),
    defaultValues: {
      email: "",
      password: "",
    },
    mode: "onChange",
  });

  const onSubmit = methods.handleSubmit(async (data) => {
    try {
      const result = await vksfso.auth.postApiAuthSignIn({ requestBody: data });
      AuthToken.save(result.accessToken);

      dispatch(viewerModel.actions.checkViewerAsync());
    } catch (error) {
      const message = handleSubmitError(error);
      setSubmitError(message);
    }
  });

  return {
    control: methods.control,
    onSubmit,
    submitError,
    isSubmitting: methods.formState.isSubmitting,
    isValid: methods.formState.isValid,
  };
}
