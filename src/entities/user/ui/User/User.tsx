import React, { useEffect } from "react";

namespace User {
  export type Props = {
    user: import("entities/user").userTypes.single.User;
  };
}

function UserComponent(props: User.Props) {
  const { user } = props;

  useEffect(() => {
    console.log(JSON.stringify(user));
  }, []);

  return <div>User</div>;
}

const User = React.memo(UserComponent);

export { User };
