import {
  RequestStatus,
  REQUEST_STATUSES,
} from "shared/constants/request-statuses";
import { single } from "../types";

export const SINGLE_USER_SLICE_NAME = "single-user";
export const INITIAL_STATE_SINGLE_USER: {
  singleUser: single.User;
  status: RequestStatus;
  error: null | string;
  queryConfig?: single.QueryConfig | null;
} = {
  singleUser: {},
  status: REQUEST_STATUSES.IDLE,
  error: null,
  queryConfig: null,
};
