import { RequestStatus } from "shared/constants/request-statuses";
import { list } from "../types";

export const USERS_LIST_SLICE_NAME = "users-list";
export const INITIAL_STATE_USERS_LIST: {
  users: list.NormalizedUsers;
  ids: number[];
  pageMeta: list.PageMeta;
  queryConfig?: list.QueryConfig;
  status: RequestStatus;
  error: null | string;
} = {
  users: {},
  ids: [],
  pageMeta: {},
  queryConfig: {},
  status: "idle",
  error: null,
};
