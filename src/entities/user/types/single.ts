import type { SingleUserDto } from "shared/api/vksfso";

type GetUserByIdParams = Parameters<
  typeof import("shared/api").vksfso.users.getApiUsers1
>;

export type QueryConfig = GetUserByIdParams[0];

export type User = SingleUserDto;
