import type { UserDtoPaged } from "shared/api/vksfso";

type GetUsersParams = Parameters<
  typeof import("shared/api").vksfso.users.getApiUsers
>;
export type QueryConfig = GetUsersParams[0];

export type User = UserDtoPaged["elements"][0];

export type PageMeta = UserDtoPaged["pageMeta"];

export type NormalizedUsers = Record<number, User>;
