import { bem } from "shared/lib";
import { USERS_LIST_SLICE_NAME } from "../config";

export const usersListBem = bem(USERS_LIST_SLICE_NAME);
