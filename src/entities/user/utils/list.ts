import { normalize, schema } from "normalizr";
import { list } from "../types";

export const usersSchema = new schema.Entity<list.User>("users");

export const normalizeUsersList = (data: list.User[]) =>
  normalize<list.User, { users: list.NormalizedUsers }>(data, [usersSchema]);
