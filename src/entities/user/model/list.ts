import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";


import { vksfso } from "shared/api";
import { ApiError } from "shared/api/vksfso";
import {
  RequestStatus,
  REQUEST_STATUSES,
} from "shared/constants/request-statuses";
import { createAsyncThunk } from "shared/lib/store";

import { INITIAL_STATE_USERS_LIST, USERS_LIST_SLICE_NAME } from "../config";
import type { list } from "../types";
import { normalizeUsersList } from "../utils";

const slice = createSlice({
  name: USERS_LIST_SLICE_NAME,
  initialState: INITIAL_STATE_USERS_LIST,
  reducers: {
    setUsers: (state, { payload }: PayloadAction<list.NormalizedUsers>) => {
      state.users = payload;
    },
    setIds: (state, { payload }: PayloadAction<number[]>) => {
      state.ids = payload;
    },
    setPageMeta: (state, { payload }: PayloadAction<list.PageMeta>) => {
      state.pageMeta = payload;
    },
    setQueryConfig: (state, { payload }: PayloadAction<list.QueryConfig>) => {
      state.queryConfig = payload;
    },
    setError: (state, { payload }: PayloadAction<string>) => {
      state.error = payload;
    },
    setStatus: (state, { payload }: PayloadAction<RequestStatus>) => {
      state.status = payload;
    },
    reset: () => {
      return INITIAL_STATE_USERS_LIST;
    },
  },
});

const getUsersListAsync = createAsyncThunk(
  `${USERS_LIST_SLICE_NAME}/getUsersListAsync`,
  async (_, thunkAPI) => {
    thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.LOADING));
    try {
      const { usersList } = thunkAPI.getState();

      const result = await vksfso.users.getApiUsers(usersList.queryConfig);
      const { elements, pageMeta } = result;
      const { entities, result: ids } = normalizeUsersList(elements);

      thunkAPI.dispatch(slice.actions.setUsers(entities.users));
      thunkAPI.dispatch(slice.actions.setIds(ids));
      thunkAPI.dispatch(slice.actions.setPageMeta(pageMeta));
      thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.SUCCEEDED));
    } catch (error) {
      if (error instanceof ApiError) {
        const message = error?.body?.detail ?? error.message;
        thunkAPI.dispatch(slice.actions.setError(message));
      } else {
        thunkAPI.dispatch(slice.actions.setError(String(error)));
      }
      thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.ERROR));
    }
  },
);

const isUsersIsEmpty = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.usersList.users,
      (users) => !Object.values(users).length,
    ),
  );

const useUsers = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.usersList.users,
      (users) => users,
    ),
  );

const useUser = (id: number) =>
  useSelector(
    createSelector(
      (state: RootState) => state.usersList.users,
      (users) => users[id],
    ),
  );

const useQueryConfig = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.usersList.queryConfig,
      (queryConfig) => queryConfig,
    ),
  );

const useStatus = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.usersList.status,
      (status) => status,
    ),
  );

const useError = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.usersList.error,
      (error) => error,
    ),
  );

export const actions = {
  ...slice.actions,
  getUsersListAsync,
};
export const stores = {
  initialState: slice.getInitialState(),
};
export const selectors = {
  isUsersIsEmpty,
  useUsers,
  useUser,
  useQueryConfig,
  useStatus,
  useError,
};

export const { reducer } = slice;
