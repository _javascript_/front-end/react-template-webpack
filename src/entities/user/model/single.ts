import { createSelector, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

import { vksfso } from "shared/api";
import { ApiError } from "shared/api/vksfso";
import {
  RequestStatus,
  REQUEST_STATUSES,
} from "shared/constants/request-statuses";
import { createAsyncThunk } from "shared/lib/store";

import { SINGLE_USER_SLICE_NAME, INITIAL_STATE_SINGLE_USER } from "../config";
import type { single } from "../types";

const slice = createSlice({
  name: SINGLE_USER_SLICE_NAME,
  initialState: INITIAL_STATE_SINGLE_USER,
  reducers: {
    setUser: (state, { payload }: PayloadAction<single.User>) => {
      state.singleUser = payload;
    },
    setError: (state, { payload }: PayloadAction<string>) => {
      state.error = payload;
    },
    setStatus: (state, { payload }: PayloadAction<RequestStatus>) => {
      state.status = payload;
    },
    setQueryConfig: (state, { payload }: PayloadAction<single.QueryConfig>) => {
      state.queryConfig = payload;
    },
  },
});

const getUserByIdAsync = createAsyncThunk(
  `${SINGLE_USER_SLICE_NAME}/getUserByIdAsync`,
  async (_, thunkAPI) => {
    thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.LOADING));
    try {
      const { singleUser } = thunkAPI.getState() as RootState;
      const result = await vksfso.users.getApiUsers1(singleUser.queryConfig);
      thunkAPI.dispatch(slice.actions.setUser(result));
      thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.SUCCEEDED));
    } catch (error) {
      if (error instanceof ApiError) {
        const message = error?.body?.detail ?? error.message;
        thunkAPI.dispatch(slice.actions.setError(message));
      } else {
        thunkAPI.dispatch(slice.actions.setError(String(error)));
      }
      thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.ERROR));
    }
  },
);

const useSingleUserError = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.singleUser.error,
      (error) => error,
    ),
  );

const useSingleUser = () =>
  useSelector(
    createSelector(
      (state: RootState) => state.singleUser.singleUser,
      (singleUser) => singleUser,
    ),
  );

export const actions = {
  ...slice.actions,
  getUserByIdAsync,
};
export const stores = {
  initialState: slice.getInitialState(),
};
export const selectors = {
  useSingleUserError,
  useSingleUser,
};

export const { reducer } = slice;
