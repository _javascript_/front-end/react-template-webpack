import { ACCESS_LEVELS, Level } from "shared/config/access";
import { RequestStatus } from "shared/constants/request-statuses";
import { UserInfo } from "../types";

export const ACCESS_SLICE_NAME = "access";
export const INITIAL_STATE_ACCESS: {
  currentAccess: Level[];
} = {
  currentAccess: [ACCESS_LEVELS.GUEST],
};

export const VIEWER_SLICE_NAME = "viewer";
export const INITIAL_STATE_VIEWER: {
  status: RequestStatus;
  info: UserInfo | null;
  access: Level[];
} = {
  status: "idle",
  info: null,
  access: [ACCESS_LEVELS.GUEST],
};
export const APP_READY_SLICE_NAME = "app-ready";
export const INITIAL_STATE_APP_READY: boolean = false;
