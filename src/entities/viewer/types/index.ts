import { vksfso } from "shared/api";

export type UserInfo = Awaited<
  ReturnType<typeof vksfso.users.getApiUsersUserInfo>
>;
