import {
  createAsyncThunk,
  createSelector,
  createSlice,
  PayloadAction,
} from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

import { vksfso } from "shared/api";
import { ACCESS_LEVELS, Level } from "shared/config/access";
import {
  RequestStatus,
  REQUEST_STATUSES,
} from "shared/constants/request-statuses";
import { AuthToken } from "shared/utils/auth-token";

import { VIEWER_SLICE_NAME, INITIAL_STATE_VIEWER } from "../config";
import { UserInfo } from "../types";
import { userRoleToAccessLevel } from "../utils";

const slice = createSlice({
  name: VIEWER_SLICE_NAME,
  initialState: INITIAL_STATE_VIEWER,
  reducers: {
    setInfo: (state, { payload }: PayloadAction<UserInfo>) => {
      state.info = payload;
    },
    setStatus: (state, { payload }: PayloadAction<RequestStatus>) => {
      state.status = payload;
    },
    setLevels: (state, { payload }: PayloadAction<Level[]>) => {
      state.access = payload;
    },
    addLevel: (state, { payload }: PayloadAction<Level>) => {
      if (state.access.includes(payload)) return;
      if (!Object.values(ACCESS_LEVELS).includes(payload)) return;
      state.access.push(payload);
    },
    removeLevel: (state, { payload }: PayloadAction<Level>) => {
      if (!state.access.includes(payload)) return;
      if (state.access.length <= 1) return;
      state.access = state.access.filter((l) => l !== payload);
    },
    setGuest: (state) => {
      state.access = [ACCESS_LEVELS.GUEST];
    },
    setUser: (state) => {
      state.access = [ACCESS_LEVELS.USER];
    },
    setAdmin: (state) => {
      state.access = [ACCESS_LEVELS.ADMIN];
    },
  },
});

const checkViewerAsync = createAsyncThunk(
  `${VIEWER_SLICE_NAME}/checkViewerAsync`,
  async (_, thunkAPI) => {
    thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.LOADING));
    try {
      const result = await vksfso.users.getApiUsersUserInfo();
      const { role } = result;
      thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.SUCCEEDED));
      thunkAPI.dispatch(slice.actions.setLevels([userRoleToAccessLevel(role)]));
      if (result) thunkAPI.dispatch(slice.actions.setInfo(result));
    } catch (error) {
      thunkAPI.dispatch(slice.actions.setStatus(REQUEST_STATUSES.ERROR));
      thunkAPI.dispatch(slice.actions.setGuest());
      thunkAPI.dispatch(slice.actions.setInfo(null));
      AuthToken.remove();
    }
  },
);

const useAccess = () => {
  return useSelector(
    createSelector(
      (state: RootState) => state.viewer.access,
      (access) => access,
    ),
  );
};

const useInfo = () => {
  return useSelector(
    createSelector(
      (state: RootState) => state.viewer.info,
      (info) => info,
    ),
  );
};

const useStatus = () => {
  return useSelector(
    createSelector(
      (state: RootState) => state.viewer.status,
      (status) => status,
    ),
  );
};

export const actions = {
  ...slice.actions,
  checkViewerAsync,
};
export const selectors = {
  useInfo,
  useAccess,
  useStatus,
};
export const { reducer } = slice;
