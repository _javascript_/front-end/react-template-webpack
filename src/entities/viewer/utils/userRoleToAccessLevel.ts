import { Level, ACCESS_LEVELS } from "shared/config/access";
import { UserInfo } from "../types";

export function userRoleToAccessLevel(role: UserInfo["role"]): Level {
  if (!role) return ACCESS_LEVELS.GUEST;
  if (role === "Administrator") return ACCESS_LEVELS.ADMIN;
  return ACCESS_LEVELS.USER;
}
