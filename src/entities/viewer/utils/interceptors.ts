import axios, { AxiosError } from "axios";

export function initInterceptors(redirect: () => void) {
  axios.interceptors.response.use(
    function (response) {
      console.log("[AXIOS]: interceptors.response => success ", response);
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    },
    function (error) {
      console.log("[AXIOS]: interceptors.response => error ", error);
      redirect();
      // if(error instanceof AxiosError) {
      //   if (error.status && error?.status === 401) {
      //     console.log({})
      //   }
      // }
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    },
  );
}
