import { getShowLeftbar } from "../utils";

export const APP_CONTEXT_SLICE_NAME = "app-context";

export const APP_CONTEXT_INITIAL_STATE: {
  showLeftbar: boolean;
} = {
  showLeftbar: getShowLeftbar(),
};
