import { createAction, createSelector, createSlice } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

import { APP_CONTEXT_SLICE_NAME, APP_CONTEXT_INITIAL_STATE } from "../config";
import { setShowLeftbar } from "../utils";

const toggleLeftbar = createAction(`${APP_CONTEXT_SLICE_NAME}/toggleLeftbar`);

const setLeftbar = createAction<{ show: boolean }>(
  `${APP_CONTEXT_SLICE_NAME}/setLeftbar`,
);

const slice = createSlice({
  name: APP_CONTEXT_SLICE_NAME,
  initialState: APP_CONTEXT_INITIAL_STATE,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(toggleLeftbar, (state) => {
      setShowLeftbar(!state.showLeftbar);
      state.showLeftbar = !state.showLeftbar;
    });
    builder.addCase(setLeftbar, (state, { payload }) => {
      setShowLeftbar(payload.show);
      state.showLeftbar = payload.show;
    });
  },
});

const useShowLeftbar = () => {
  return useSelector(
    createSelector(
      (state: RootState) => state.appContext.showLeftbar,
      (showLeftbar) => showLeftbar,
    ),
  );
};

export const actions = {
  ...slice.actions,
  toggleLeftbar,
  setLeftbar,
};
export const selectors = {
  useShowLeftbar,
};

export const { reducer } = slice;
