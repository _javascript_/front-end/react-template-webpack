import { getRouteConfigs } from "shared/lib/router";

import { ERRORS } from "./errors";
import { EXAMPLE } from "./example";
import { HOME } from "./home";
import { SIGN_IN } from "./sign-in";
import { SIGN_UP } from "./sign-up";

const pages = [HOME, ERRORS, SIGN_IN, SIGN_UP, EXAMPLE];

export const ROUTE_CONFIGS = getRouteConfigs(pages);
