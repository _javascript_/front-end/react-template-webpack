import React from "react";

import { Navigate, Routes, Route } from "react-router";

import { PAGE_NAMES, paths } from "shared/config/pages";

import { Footer } from "widgets/footer";
import { Header } from "widgets/header";
import { Layout } from "widgets/layout";
import { Leftbar } from "widgets/left-bar";

import { viewerModel } from "entities/viewer";
import { REQUEST_STATUSES } from "shared/constants/request-statuses";
import { useGuardRoutes, redirectsRouteObject } from "shared/lib/router";
import { Loader } from "shared/ui";

import { ROUTE_CONFIGS } from "./config";

const NOT_FOUND = paths[PAGE_NAMES.ERRORS]("404");

export function Routing() {
  const access = viewerModel.selectors.useAccess();
  const status = viewerModel.selectors.useStatus();

  const routes = useGuardRoutes(ROUTE_CONFIGS, access);

  if (status === REQUEST_STATUSES.LOADING || status === REQUEST_STATUSES.IDLE) {
    return <Loader key="loader" variant="circul" />;
  }

  return (
    <Routes key="routes">
      <Route
        element={<Layout Footer={Footer} Header={Header} Leftbar={Leftbar} />}
      >
        {[...redirectsRouteObject, ...routes].map(
          ({ element, index, path }) => {
            return (
              <Route key={path} path={path} index={index} element={element} />
            );
          },
        )}
      </Route>
      <Route path="*" element={<Navigate to={NOT_FOUND} replace />} />
    </Routes>
  );
}
