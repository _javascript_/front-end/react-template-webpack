import React from "react";
import { useNavigate } from "react-router";

import { usePage } from "pages/utils";
import { router } from "shared/lib";
import { Button, Link, Page } from "shared/ui/atoms";


import { PageTitle } from "./components";

function Example(props: router.InjectionProps) {
  const { helmet, title } = usePage(props);
  const navigate = useNavigate();

  return (
    <Page helmet={helmet}>
      <PageTitle title={title} />
      <Link to="access">Access</Link>
      <Link to="layouts">Layouts</Link>
      <Link to="todos">Todos</Link>
      <Button onClick={() => navigate(-1)}>Go to Back</Button>
    </Page>
  );
}

export default Example;
