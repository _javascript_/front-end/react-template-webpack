import { useEffect } from "react";

import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";

import { breadcrumbsConfig, breadcrumbsModel } from "features/breadcrumbs";

import { i18n, router } from "shared/lib";

export function usePage(props: router.InjectionProps) {
  const dispatch = useDispatch();
  const helmet = i18n.useHelmetTranslation(props.metadata.pageName);
  const { t } = useTranslation("pages");

  const title = t(props.metadata.pageName);

  useEffect(() => {
    const items = breadcrumbsConfig.BREADCRUMBS[props.metadata.pageName] ?? [];
    dispatch(breadcrumbsModel.actions.set(items));
    return () => {
      dispatch(breadcrumbsModel.actions.reset());
    };
  }, []);

  return { helmet, title };
}
