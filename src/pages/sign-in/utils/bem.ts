import { PAGE_NAMES } from "shared/config/pages";
import { bem as bemLib} from "shared/lib/bem";

export const CSS_PAGE_NAME = `page-${PAGE_NAMES.SIGN_IN}`;
export const bem = bemLib(CSS_PAGE_NAME);

