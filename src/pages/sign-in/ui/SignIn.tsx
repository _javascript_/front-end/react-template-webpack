import React from "react";

import { usePage } from "pages/utils";
import { SignInForm } from "features/auth/sign-in";
import { router } from "shared/lib";
import { Page } from "shared/ui/atoms";

function SignIn(props: router.InjectionProps) {
  const { helmet } = usePage(props);

  return (
    <Page helmet={helmet}>
      <SignInForm className="page-sign-in__form" />
    </Page>
  );
}

export default SignIn;
