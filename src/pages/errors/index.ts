import React from "react";
import { PAGE_NAMES, PageConfig } from "shared/config/pages";

const Page = React.lazy(() => import("./ui"));

export const ERRORS: PageConfig = {
  pageName: PAGE_NAMES.ERRORS,
  component: Page,
};

