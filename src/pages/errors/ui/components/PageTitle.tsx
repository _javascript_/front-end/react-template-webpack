import React from "react";

import { clsx } from "shared/lib";
import { Typography } from "shared/ui";

import { bem } from "../../utils";

namespace PageTitle {
  export type Props = {
    title: string;
  };
}
function PageTitle(props: PageTitle.Props) {
  const { title } = props;

  const classes = clsx(bem("title"));

  return (
    <Typography className={classes} variant="h3">
      {title}
    </Typography>
  );
}

export { PageTitle };

