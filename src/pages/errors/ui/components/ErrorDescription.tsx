import React from "react";

import { ERRORS_CODE, Code } from "shared/constants/errors-code";
import { clsx } from "shared/lib";
import { Typography } from "shared/ui";

import { bem } from "../../utils";

export namespace ErrorDescription {
  export type Props = {
    code: Code;
  };
}

export function ErrorDescription(props: ErrorDescription.Props) {
  const { code } = props;

  const description = ERRORS_CODE[code] ?? ERRORS_CODE[404];
  const classes = clsx(bem());

  return <Typography className={classes}>{description}</Typography>;
}
