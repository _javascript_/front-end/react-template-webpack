import React from "react";

import { PAGE_NAMES, paths } from "shared/config/pages";
import { useAppNavigate } from "shared/hooks/useAppNavigate";
import { clsx } from "shared/lib";
import { Button, Link } from "shared/ui";

import { bem } from "../../utils";

export namespace Actions {}

export function Actions() {
  const navigate = useAppNavigate();
  const classes = clsx(bem("actions"));
  return (
    <div className={classes}>
      <Link to={paths[PAGE_NAMES.HOME]()}>На главную</Link>
      <Button onClick={() => navigate.goToBack()}>Назад</Button>
    </div>
  );
}
