import React from "react";
import { useParams } from "react-router";

import { usePage } from "pages/utils";
import { Code } from "shared/constants/errors-code";
import { router } from "shared/lib";
import { Page } from "shared/ui/atoms";


import { PageTitle, Actions, ErrorDescription } from "./components";

function Errors(props: router.InjectionProps) {
  const { helmet, title } = usePage(props);
  const { code } = useParams();

  return (
    <Page helmet={helmet}>
      <PageTitle title={`${title} ${code}`} />
      <ErrorDescription code={Number(code) as Code} />
      <Actions />
    </Page>
  );
}

export default Errors;
