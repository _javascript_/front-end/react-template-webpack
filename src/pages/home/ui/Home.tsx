import React, { useEffect } from "react";

import { usePage } from "pages/utils";
import { usersListModel } from "entities/user";
import { router } from "shared/lib";
import { useAppDispatch } from "shared/lib/store";
import { Page } from "shared/ui/atoms";



import { PageTitle } from "./components";

function Home(props: router.InjectionProps) {
  const dispatch = useAppDispatch();

  const { helmet, title } = usePage(props);

  const query = usersListModel.selectors.useQueryConfig();
  const status = usersListModel.selectors.useStatus();
  const error = usersListModel.selectors.useError();

  useEffect(() => {
    dispatch(usersListModel.actions.getUsersListAsync());
  }, []);

  return (
    <Page helmet={helmet}>
      <PageTitle title={title} />
      {status}
      {error && <p>{error}</p>}
      <br />
      {JSON.stringify(query)}
    </Page>
  );
}

export default Home;
