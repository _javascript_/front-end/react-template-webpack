import React from "react";
import { PAGE_NAMES, PageConfig } from "shared/config/pages";

const Page = React.lazy(() => import("./ui"));

export const HOME: PageConfig = {
  pageName: PAGE_NAMES.HOME,
  component: Page,
};

