import React from "react";

import { usePage } from "pages/utils";
import { router } from "shared/lib";
import { Page } from "shared/ui/atoms";


import { PageTitle } from "./components";

function SignUp(props: router.InjectionProps) {
  const { helmet, title } = usePage(props);

  return (
    <Page helmet={helmet}>
      <PageTitle title={title} />
    </Page>
  );
}

export default SignUp;

