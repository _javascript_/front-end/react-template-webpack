import React from "react";
import { PAGE_NAMES, PageConfig } from "shared/config/pages";

const Page = React.lazy(() => import("./ui"));

export const SIGN_UP: PageConfig = {
  pageName: PAGE_NAMES.SIGN_UP,
  component: Page,
};
