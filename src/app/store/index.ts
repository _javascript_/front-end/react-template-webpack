import { configureStore } from "@reduxjs/toolkit";

import { breadcrumbsModel } from "features/breadcrumbs";
import { selectedItemsModel } from "features/selected-items";
import { selectedUsersModel } from "features/user/selected-users";
import { appContextModel } from "entities/app-context";
import { usersListModel, singleUserModel } from "entities/user";
import { viewerModel } from "entities/viewer";

export const store = configureStore({
  reducer: {
    appContext: appContextModel.reducer,
    viewer: viewerModel.reducer,
    breadcrumbs: breadcrumbsModel.reducer,
    usersList: usersListModel.reducer,
    singleUser: singleUserModel.reducer,
    selectedUsers: selectedUsersModel.reducer,
    selectedItems: selectedItemsModel.reducer,
  },
});
