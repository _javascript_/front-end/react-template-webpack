import React from "react";


import { CheckViewer } from "processes/check-viewer";
import { ErrorInterceptor } from "processes/interceptors";
import { Routing } from "pages";

import { withProviders } from "./providers";

function App() {
  return (
    <>
      <CheckViewer />
      <ErrorInterceptor />
      <Routing />
    </>
  );
}

export default withProviders(App);
