import React from "react";

import { store } from "app/store";

import { StoreProvider } from "shared/lib/store";

export function withStore(component: () => React.ReactNode) {
  return function WidthStore() {
    return <StoreProvider store={store}>{component()}</StoreProvider>;
  };
}
