import React from "react";
import { AccessControlPanelPortal } from "widgets/access-control-panel";
import { IS_DEV_ENV } from "shared/config/application";

export const withDev = (component: () => React.ReactNode) =>
  function WithDev() {
    return (
      <>
        {component()}
        {IS_DEV_ENV && <AccessControlPanelPortal />}
      </>
    );
  };
