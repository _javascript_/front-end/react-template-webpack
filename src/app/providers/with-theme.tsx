import React from "react";
import { ThemeProvider } from "shared/lib/theme";

export function withTheme(component: () => React.ReactNode) {
  return function WithTheme() {
    return <ThemeProvider>{component()}</ThemeProvider>;
  };
}
