import React from "react";
import { HashRouter, BrowserRouter } from "react-router-dom";
import { IS_HASH_ROUTER } from "shared/config/application";

const Provider = IS_HASH_ROUTER ? HashRouter : BrowserRouter;

export const withRouter = (component: () => React.ReactNode) => {
  return function RouterProvider() {
    return <Provider>{component()}</Provider>;
  };
};
