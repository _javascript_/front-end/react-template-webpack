import compose from "compose-function";

import { withHelmet } from "./with-helmet";
import { withRouter } from "./with-router";
import { withStore } from "./with-store";
import { withTheme } from "./with-theme";

import "shared/assets/styles/index.css";

export const withProviders = compose(
  withStore,
  withRouter,
  withHelmet,
  withTheme,
);
