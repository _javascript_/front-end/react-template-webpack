import React from "react";
import { HelmetProvider } from "react-helmet-async";

export function withHelmet(component: () => React.ReactNode) {
  return function WithHelmet() {
    return <HelmetProvider>{component()}</HelmetProvider>;
  };
}
