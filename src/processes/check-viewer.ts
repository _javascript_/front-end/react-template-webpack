import { useEffect } from "react";

import { viewerModel } from "entities/viewer";
import { useAppDispatch } from "shared/lib/store";

export function useCheckViewer() {
  const dispath = useAppDispatch();

  useEffect(() => {
    dispath(viewerModel.actions.checkViewerAsync());
  }, []);
}

export function CheckViewer(): any {
  useCheckViewer();
  return null;
}
