import { AxiosError } from "axios";
import { useEffect } from "react";

import { initErrorInterceptor } from "shared/api/interceptors";

export function useErrorInterceptor() {
  useEffect(() => {
    const cb = (error: AxiosError<{ detail: string }>) => {
      console.log("[ErrorInterceptor]: cb", error);
      // const code = String(error.response.status);
      // const detail = error.response.data?.detail ?? error.response.statusText;

      // const path =
      //   code === "401"
      //     ? paths[PAGE_NAMES.SIGN_IN]()
      //     : paths[PAGE_NAMES.ERRORS](code);

      // const state = { detail, prevPath: location.pathname };

      // navigate(path, { state });
    };

    initErrorInterceptor(cb);
  }, []);
}

export function ErrorInterceptor(): any {
  useErrorInterceptor();
  return null;
}
