import { RequestStatus, REQUEST_STATUSES } from "shared/constants/request-statuses";

export const {{ENTITY_NAME}}_SLICE_NAME = "{{entity-name}}";

export const INITIAL_STATE_{{ENTITY_NAME}}: {
  status: RequestStatus;
  error: null | string;
  list: list.NormalizedUsers;
  ids: number[];
} = {
  status: REQUEST_STATUSES.IDLE,
  error: null,
  list: {},
  ids: [],
};
