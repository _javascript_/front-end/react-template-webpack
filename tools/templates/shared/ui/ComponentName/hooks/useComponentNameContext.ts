import  { useContext } from 'react';

import { {{ComponentName}}Context } from '../{{ComponentName}}Context';

export function use{{ComponentName}}Context() {
  return useContext({{ComponentName}}Context);
}