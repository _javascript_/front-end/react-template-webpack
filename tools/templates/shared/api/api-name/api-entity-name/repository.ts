import { customFetch } from "../base";

import * as dto from "./dto";

class {{ApiEntityName}}Repository {
  private baseUrl = "/{{api-controller-path}}";

  private fetch = customFetch;

  public async getAll{{ApiEntityName}}s
    (dto: dto.GetAll{{ApiEntityName}}sReqDto
  ): Promise<dto.GetAll{{ApiEntityName}}sResDto> {
    return this.fetch.get(this.baseUrl);
  }

  public async getOne{{ApiEntityName}}
    (dto: dto.GetOne{{ApiEntityName}}ReqDto
  ): Promise<dto.GetOne{{ApiEntityName}}ResDto> {
    return this.fetch.get(this.baseUrl);
  }

  public async get{{ApiEntityName}}Paged
    (dto: dto.Get{{ApiEntityName}}PagedReqDto
  ): Promise<dto.Get{{ApiEntityName}}PagedResDto> {
    return this.fetch.get(this.baseUrl);
  }

  public async create{{ApiEntityName}}
    (dto: dto.Create{{ApiEntityName}}ReqDto
  ): Promise<dto.Create{{ApiEntityName}}ResDto> {
    return this.fetch.post(this.baseUrl);
  }

  public async update{{ApiEntityName}}
    (dto: dto.Update{{ApiEntityName}}ReqDto
  ): Promise<dto.Update{{ApiEntityName}}ResDto> {
    return this.fetch.patch(this.baseUrl);
  }

  public async delete{{ApiEntityName}}
    (dto: dto.Delete{{ApiEntityName}}ReqDto
  ): Promise<dto.Delete{{ApiEntityName}}ResDto> {
    return this.fetch.delete(this.baseUrl);
  }
}

export const repo = new {{ApiEntityName}}Repository();
