export type Delete{{ApiEntityName}}ReqDto = {
  id: string;
};