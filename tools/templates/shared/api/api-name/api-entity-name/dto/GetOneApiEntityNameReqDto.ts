export type GetOne{{ApiEntityName}}ReqDto = {
  id: number;
};