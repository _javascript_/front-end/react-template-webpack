export * from "./GetAll{{ApiEntityName}}sReqDto";
export * from "./GetAll{{ApiEntityName}}sResDto";

export * from "./GetOne{{ApiEntityName}}ReqDto";
export * from "./GetOne{{ApiEntityName}}ResDto";

export * from "./Get{{ApiEntityName}}PagedReqDto";
export * from "./Get{{ApiEntityName}}PagedResDto";

export * from "./Create{{ApiEntityName}}ReqDto";
export * from "./Create{{ApiEntityName}}ResDto";

export * from "./Delete{{ApiEntityName}}ReqDto";
export * from "./Delete{{ApiEntityName}}ResDto";

export * from "./Update{{ApiEntityName}}ReqDto";
export * from "./Update{{ApiEntityName}}ResDto";
