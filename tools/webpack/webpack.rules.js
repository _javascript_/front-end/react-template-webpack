/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/order */
const { inDev } = require("./webpack.helpers");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = [
  {
    // Typescript loader
    test: /\.tsx?$/,
    exclude: /(node_modules|\.webpack)/,
    use: {
      loader: "ts-loader",
      options: {
        transpileOnly: true,
      },
    },
  },
  {
    // CSS Loader
    test: /\.css$/,
    use: [
      { loader: inDev() ? "style-loader" : MiniCssExtractPlugin.loader },
      { loader: "css-loader" },
    ],
  },
  {
    // Scss loader
    test: /\.scss$/,
    use: [
      { loader: inDev() ? "style-loader" : MiniCssExtractPlugin.loader },
      { loader: "css-loader" },
      { loader: "sass-loader" },
    ],
  },
  // Images Loader
  // Font & SVG loader
  {
    test: /\.(jpe?g|svg|png|gif|ico|eot|ttf|woff2?)(\?v=\d+\.\d+\.\d+)?$/i,
    type: "asset/resource",
  },
];
