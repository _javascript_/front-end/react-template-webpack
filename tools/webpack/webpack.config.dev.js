const { isHttps, getPort, getHostname } = require("./webpack.helpers");

module.exports = {
  mode: "development",
  entry: ["./src/main.tsx"],
  module: {
    rules: require("./webpack.rules"),
  },
  output: {
    publicPath: "/",
    filename: "[name].js",
    chunkFilename: "[name].chunk.js",
  },
  plugins: require("./webpack.plugins"),
  resolve: {
    extensions: [".js", ".ts", ".jsx", ".tsx", ".css"],
    alias: {
      // Custom Aliases
      ...require("./webpack.aliases"),
    },
  },
  stats: "errors-warnings",
  devtool: "cheap-module-source-map",
  devServer: {
    open: true,
    hot: true,
    // host: getHostname(),
    https: isHttps(),
    port: getPort(),
    historyApiFallback: true,
    // proxy: {
    //   '/api': 'http://localhost:3080',
    // },
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  performance: {
    hints: false,
  },
};
