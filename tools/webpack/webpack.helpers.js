/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */

const fs = require("fs");
const path = require("path");

const ROOT = process.cwd();
const SRC_PATH = path.join(ROOT, "src");

const https = {
  key: path.join(ROOT, "tools", "cert", "key.pem"),
  cert: path.join(ROOT, "tools", "cert", "cert.pem"),
};

const dirs = fs.readdirSync(SRC_PATH).filter((dir) => {
  return !dir.split(".")[1];
});

function createWebpackAliases() {
  return dirs.reduce((acc, dir) => {
    acc[dir] = path.join(SRC_PATH, dir);
    return acc;
  }, {});
}

function isHttps() {
  const result = process.env.PROTOCOL === "https" ? https : undefined;
  return result;
}

function inDev() {
  return process.env.NODE_ENV === "development";
}

function getPort() {
  return process.env.PORT ?? 8080;
}

function getHostname() {
  return process.env.HOSTNAME ?? undefined;
}

module.exports = {
  ROOT,
  SRC_PATH,
  inDev,
  isHttps,
  getPort,
  getHostname,
  createWebpackAliases,
};
