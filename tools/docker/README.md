DOCKER IMAGES
sudo docker image ls

DOCKER CONTAINERS
sudo docker ps

DOCKER SEE CONTAINER
docker exec -it {container name} bash
sudo docker exec -ti docker-react-app-1 /bin/sh

DOCKER RM
sudo docker rm {container name} -f

DOCKER START PROD
sudo docker compose -f docker-compose.yml -f docker-compose-prod.yml up -d --build
DOCKER STOP PROD
sudo docker compose -f docker-compose.yml -f docker-compose-prod.yml down

DOCKER START DEV
sudo docker compose -f docker-compose.yml -f docker-compose-dev.yml up -d --build
DOCKER STOP DEV
sudo docker compose -f docker-compose.yml -f docker-compose-dev.yml down

DOCKER DELETE ALL IMAGES
sudo docker rmi $(sudo docker images -a -q)

DOCKER DELETE ALL CONTAINERS
sudo docker stop $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)
